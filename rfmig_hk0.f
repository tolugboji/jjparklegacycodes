c  program rfmig_hk0
c  10/15/13 JJP 
c  10/12/04 JJP -- adapted from rfmigrate 
c  does radial/transverse phase-shift stack
c
c  xf77 -o /Users/jjpark/bin/rfmig_hk0 rfmig_hk0.f /Users/jjpark/Plotxy/plotlib.a /Users/jjpark/Ritz/eislib.a /Users/jjpark/Ritz/jlib.a
c
c  computes moving-window moveout correction for MTC receiver functions 
c  applied in the frequency domain.
c  requires a stacking model in the anirec format
c  such a model may have anisotropy parameters, 
c  but migration code only uses the isotropic velocities. 
c
c
c  has kluge to cheat the pre-event noise for synthetic records  3/12/00 JJP
c  check to see if the kluge is commented out
c
c  this version of the RF code reads a file of data filenames 
c  you have two choices: either read the time intervals in the filename file
c  or read them in the sac header
c  the data must be binary SAC format
c  horizontals must be rotated to radial and transverse
c
c  for start times in the file:
c  the file is "in_recfunk" and has lines of the form:
c
c  1997.070.19.33.bh?  <-- code replaces ? with r,t,z
c  57 52 	       <-- start time of analysis window, window duration (sec)
c  1997.076.08.15.bh?
c  62 62 
c  ...
c  ...
c  ...
c  stop                <-- tells code that data is finished, 799 events max
c
c
c  for start times in the SAC header
c  reads seismic record start times from the sac header
c  will search the SAC header for specific markers of P phases
c  T1 - P, Pdiff    ahead(12)
c  T2 - PKP,PKIKP   ahead(13)
c  T3 - PP          ahead(14)
c  T1=T2=T3=0 ==> use original A-marker ahead(9)
c
c   code does NOT combine data with different sample rates
c  data files limited to 99K pnts. To increase, see common block /datastuff/
c
c   many intermediate quantities are plotted with PLOTIT as the code proceeds.
c  other intermediate quantities can be plotted by uncommenting calls to PLOTIT
c
      implicit real*4(a-h,o-z)
      implicit integer*4(i-n)
      real*8 ar,ai,el(12),aaa,bootgg,bootdd,bootmm
      complex*8 zc,zero,bootd,bootm,bootma,bootmv,cdev,zone,eye
      complex*8 afft,rf,rfs,zramp
      character*80 name,subtext,string,name_stack
      character*28 title(2)
      character*10 cmps(3)
      character*18 xlabel(2)
      character*14 xlabel2(2)
      character*12 xlabel3(2)
      character comp(3)
      common/nnaamm/iwflag
      common/npiprol/anpi
      common/moovwin/nst(3),tsh(3,799),vpvs(21),hh(21),hkstak(21,21) 
      common/stap2/tap(16400,16)
      common/taperz/ta(16400,8,3),tai(16400,8,3)
      common/stap0/ar(16400),ai(16400),bb(16400),time(99000)
      common/specs/afft(4100,8,3),rf(4100,6),sss(4100,6),crf(4100,2)
      common/saveit/rft(4100,799,2),crft(4100,2),rfs(4100,799,6),
     x   drfs(4100,799,6),bazs(799),gcarc(799),iflag(799),
     x                         epic(799),s2n(799),ip(799),facc(4100)
      common/datastuff/a(99000,3),pdat(16400),tdat(16400),drf(4100,6)
      common/dataspln/aa(99000),aas(99000),alq(99000,3)
      common/boot/bootg(10,10,4100),bootd(10,4100),
     x            bootm(500,10,4100)
      common/boot1/bootma(10,4100),bootmv(10,10,4100)
      common/boot2/bootgg(10,10),bootdd(10),bootmm(10,2),bf(10),cdev(10)
      common/hkstak/zramp(4100),xstak(21,21),ystak(21,21)
c NOTE THAT MODEL PARAMETERS ARE REAL*4 IN THIS CODE, NOT REAL*8 AS IN ANIREC
c ONLY 20 LAYERS OVER HALFSPACE ARE ALLOWED
      common/model/z(21),dz(21),rho(21),vp(21),vs(21),vp2(21),
     x               vp4(21),vs2(21),vss(21)
      common/model2/xmu(21),xla(21),xmu2(21),xla2(21),xla4(21)
      common/modstk/tau(21),itau(21),psd(21),gam(21),xik(21)
      common/tt_p/d_p(200),d_pkp(200),s_p(200),s_pkp(200),n_p,n_pkp
      common/header/ahead(158)
      dimension iah(158),chead(158),mmonth(12),fr(3),tt(3),ttt(3),
     x  xx(2),yy(2),pfac(3),pulse(3),dtsh(3)
      equivalence (ahead,iah),(ahead,chead)
      data mmonth/0,31,59,90,120,151,181,212,243,273,304,334/
      data comp/'r','t','z'/
      data cmps/'Radial    ','Transverse','Vertical  '/
      data title/'Radial Receiver Function    ',
     x 'Transverse Receiver Function'/
      data xlabel/'H\\sub{R}(f) phase','H\\sub{T}(f) phase'/
      data xlabel2/'|H\\sub{R}(f)|','|H\\sub{T}(f)|'/
      data xlabel3/'H\\sub{R}(t)','H\\sub{T}(t)'/
      pi=3.14159265358979
      con=180./pi
      pih=pi/2.
      pi2=pi*2.0
      zero=cmplx(0.,0.)
      zone=cmplx(1.,0.)
      eye=cmplx(0.,1.)
      sq2=sqrt(2.)
      fmax=5.
c  read_ttimes reads the info for moveout correction form ttimes output file
c  write_ttimes, reads ttimes files, then writes out results in a data-statement format
c  fill_ttimes uses those data statements for the moveout corrections, allowing the code to be portable
c      call write_ttimes
c      call read_ttimes
      call fill_ttimes
c      print *,'enter sampling time e.g., 0.05 for 20 sps'
c      read(5,*) dt
      print *,'hardwired to 50 sps -- data will be splined if 20sps'
      dt0=0.02
c      print *,'hardwired to 100 sps -- data will be splined if 20sps'
c      dt0=0.01
c      print *,'hardwired to 20 sps'
c      dt0=0.05
      print *,'good for data, bad for noiseless synthetics'
      print *,'use inverse-variance weighting? (0=no)'
      read(5,*) invwgt
c  hardwire a starting one-layer model for now
      nl=1
c  read in theta,phi in degrees - polar coords of fast axis
      nlp=2
      nlm=0
c  the code uses generic crust and rock elastic and density values
c  the user is asked for a target depth in km
      rho(1)=2300.
      rho(2)=3200.
      vs(1)=3500.
      vs(2)=4400.
      vp(1)=6200.
      vp(2)=8200.
      xmu(1)=rho(1)*vs(1)**2
      xmu(2)=rho(2)*vs(2)**2
      xla(1)=rho(1)*vp(1)**2
      xla(2)=rho(2)*vp(2)**2
      print *,'enter approximate Moho depth in km'
      read(5,*) hmoho
      z(1)=hmoho*1000.
c      z(1)=35000.
      z(2)=90000.
c      do i=2,nl
c        dz(i)=z(i)-z(i-1)
c      end do
      dz(1)=z(1)
      sdelay=0.
      pdelay=0.
      smsdelay=0.
      pmsdelay=0.
      do i=1,nl
        sdelay=sdelay+(dz(i)/vs(i))
        pdelay=pdelay+(dz(i)/vp(i))
	smsdelay=smsdelay+(dz(i)/vp(i))+2.*(dz(i)/vs(i))
	pmsdelay=pmsdelay+2.*(dz(i)/vp(i))+(dz(i)/vs(i))
        tau(i)=sdelay-pdelay
        itau(i)=tau(i)/dt0
        print *,itau(i),tau(i)
      end do
      itau(nlp)=4100
      print *, 'organ-pipe mode count at 1 Hz in stack of layers: S & P'
      print *,'These are the S & P traveltimes from the basal interface'
      print 104,sdelay,pdelay
c      print *,'enter the depth at which you wish to focus the RFs (km)'
c      read(5,*) itarget
c      targm=itarget*1000.
c      do i=nl,1,-1
c        resz=targm-z(i)
c	vdelay=tau(i)
c        if(targm.gt.z(i)) go to 313
c      end do
c      i=0
c      resz=targm
c      vdelay=0.
c  313 idelay=i+1
c      vdelay=vdelay+resz/vs(idelay)-resz/vp(idelay)
c      print *,'Ps delay at vertical incidence = ',vdelay,' sec'
c      vdelay=vdelay+resz/vs(idelay)-resz/vp(idelay)
      
      print *,'Ps delay at vertical incidence = ',tau(nl),' sec'
      aaa=pmsdelay-pdelay
      print *,'Ppms delay at vertical incidence = ',aaa,' sec'
      aaa=smsdelay-pdelay
      print *,'Psms delay at vertical incidence = ',aaa,' sec'
  104 format(2f10.1)
c      print *,'enter fmax (Hz)'
c      read(5,*) fmax
c  we set fmax=6.0 Hz to store the RFs for later processing
c  the HK stack is applied at specified frequency cutoffs
c  with npad=16384, dt=0.02sec, nf.approx.1966
      fmax=5.0
c  some default values
c generate tapers for npad=16384 point records
      anpi=2.5
      nwin=3
      npad=16384
      nnf=npad/2
      do i=1,4100
        crft(i,1)=0.
        crft(i,2)=0.
      end do
      irec=0
 1010 print *,'User-input time windows (0) or sac-header picks (1)'
      read(5,*) isegment
      if(isegment.eq.0) then
        print *,
     x'file of seismic records to read? (space-ret: in_recfunk)'
        read(5,101) name
        if(name(1:1).eq.' ') then
          open(10,file='in_recfunk',form='formatted')
        else
          open(10,file=name,form='formatted')
        endif
      elseif(isegment.eq.1) then
        print *,
     x'file of seismic records to read? (space-ret: in_recpick)'
        read(5,101) name
        if(name(1:1).eq.' ') then
          open(10,file='in_recpick',form='formatted')
        else
          open(10,file=name,form='formatted')
        endif
	print *,'duration of data windows for analysis'
	read(5,*) postwin
      else
        go to 1010
      endif
c  kludge to check for effectiveness of less padding
      npad=4096
      nnf=npad/2
c      print *,'rotate to LQT coordinates to isolate P and SV? (0=no)'
c      read(5,*) lqt
      lqt=1
   10 print *,'enter filename e.g. 1998.361.bhz'
      read(10,101) name
  101 format(a)
      if(name(1:4).eq.'stop') go to 111
c  we assume that the last character is either 'r','t',or 'z'
      do i=80,1,-1
        if(name(i:i).ne.' ') then
          namlen=i
          go to 98
        endif
      end do
      print *,name
      print *,'no filename?'
      stop
   98 continue
      do kcmp=1,3
        name(namlen:namlen)=comp(kcmp)
        print *,name
        call sacread(name,a(1,kcmp),ierr)
        dt=ahead(1) 
        nscan=iah(80)
        if(nscan.gt.99000) then
          print *,'CAREFUL! Data length is greater than 99000!', nscan
          stop
        endif
	if(abs(dt/dt0-1.0).gt.0.01) then  
c  assume that dt>dt0 -- only a problem for 100 sps data
	  do i=1,nscan
	    aa(i)=a(i,kcmp)
	  end do
	  call splneq(nscan,aa,aas)
	  dxt=dt0/dt
	  xt=1.0
	  j=1
	  do while(xt.le.float(nscan))
	    a(j,kcmp)=evaleq(xt,nscan,aa,aas,0.,1.)
	    xt=xt+dxt
	    j=j+1
	  end do
	  nscan=j
	  dt=dt0
	endif
        tt(1)=0.
        tt(2)=dt
        tt(3)=dt	  
      end do
      if(nscan.gt.99000) then
        print *,'SPLINED Data length is greater than 99000!', nscan
        stop
      endif
      irec=irec+1
      if(irec.gt.799) then
        print *,'max records exceeded'
	stop
      endif
      npre=10./dt
      npost=30./dt
      ntot=npre+npost
      ttt(1)=-npre*dt
      ttt(2)=dt
      ttt(3)=dt
      do i=1,ntot
        time(i)=-ttt(1)-(i-1)*ttt(2)
      end do
      baz=ahead(53)
      bazs(irec)=ahead(53)
      gcarc(irec)=ahead(54)
      epic(irec)=ahead(54)
c      do i=0,79,10
c        print *,(ahead(ii+i),ii=1,10)
c      end do
      print *,bazs(irec),'= back azim ',ahead(54),'= epicentral dist'
      if(ahead(53).lt.-0.1.or.ahead(53).gt.360.1) then
        print *,'HEY! THE BACK AZIMUTH OF THIS RECORD IS OUT OF RANGE!'
        stop
      elseif(ahead(54).lt.-0.1.or.ahead(54).gt.180.1) then
        print *,'HEY! THE GCARC OF THIS RECORD IS OUT OF RANGE!'
        stop
      endif
c  ddf is cycles/day
      ddf=1./(dt*npad)
      nf=fmax/ddf+1
      fr(1)=0.
      fr(2)=ddf
      fr(3)=ddf
      print *,ddf,nf,fmax,npad,dt,(fr(i),i=1,3)
C  pre and post-window durations are the same
c  program pads with zeroes if start time is too early
      if(isegment.eq.0) then
        print *,'P window start time, duration (sec)'
        read(10,*) pstart,postwin
	imark=0
      else
c   READING TIME INTERVAL FROM SAC HEADER
c  ahead(12) is SAC's T1 parameter, for arrival time.  Set by pressing T & 1 in plotpk
        if(ahead(12).gt.1) then
          tmark=ahead(12)
	  imark=1
c  ahead(13) is SAC's T2 parameter, for arrival time.  Set by pressing T & 2 in plotpk
        elseif(ahead(13).gt.1) then
          tmark=ahead(13)
	  imark=2
c  ahead(14) is SAC's T3 parameter, for arrival time.  Set by pressing T & 3 in plotpk
        elseif(ahead(14).gt.1) then
          tmark=ahead(14)
	  imark=3
c  ahead(9) is SAC's A parameter, for arrival time.  Set by pressing A in plotpk
        elseif(ahead(9).gt.1) then
          tmark=ahead(9)
	  imark=0
        else
          print *,' hey! no P-wave mark for file ',name
	  stop
        endif
        pstart=tmark-ahead(6)-3.
        print *,'sac params b and tmark, pstart',ahead(6),tmark,pstart
      endif
      npts=postwin/dt
      epipi=epic(irec)
c  preferred option -- use parameterized relation between GCARC and zrrot
c  compute approximate angle between Z and R components for P wave
c  if the phase is PP, its moveout is the same as a P wave at gcarc/2
      iflag(irec)=1
      if(imark.eq.3) then
	epipi=epipi/2.
	gcarc(irec)=epipi
      endif
      if(imark.eq.2) then
	zrrot=9.0
	iflag(irec)=2
      else
        if(epipi.lt.80.) then
          zrrot=42.+(20.-epipi)*(20./60.)
        elseif(epipi.lt.118.) then
          zrrot=16.+(6./900.)*(110.-epipi)**2
        elseif(imark.eq.0) then
          zrrot=9.0
	  iflag(irec)=2
	else
          zrrot=16.+(6./900.)*(110.-epipi)**2
	endif
      endif
c  get p-slowness -- iflag branches on the start time of the analysis window
      slow=pslow(epic(irec),iflag(irec))
c  the stack-model velocities are in SI units, s_p is in sec/km
      slw=(slow/1000.)       
      tshft=0.
      tshft_sms=0.
      tshft_pms=0.
      do k=1,nl
        coss=sqrt(1.-vs(k)*vs(k)*slw*slw)
        cosp=sqrt(1.-vp(k)*vp(k)*slw*slw)
c  trapdoor to avoid incoming slownesses that are evanescent (e.g. head waves)
c  basically, one stacks with the last acceptable interval velocity
        tshft=tshft+dz(k)*(coss/vs(k)-cosp/vp(k))
        tshft_sms=tshft_sms+dz(k)*2.0*(coss/vs(k))
        tshft_pms=tshft_pms+dz(k)*(coss/vs(k)+cosp/vp(k))
      end do
      tsh(1,irec)=tshft
      tsh(2,irec)=tshft_pms
      tsh(3,irec)=tshft_sms
      print *,'record ',irec,'  time shifts',(tsh(j,irec),j=1,3)
c  here is the option to rotate the Radial and vertical Components to
c  a P and SV coordinate system    
      if(lqt.ne.0) then
        zrrot=zrrot/con
        cs=cos(zrrot)
        sn=sin(zrrot)
c  rotate from Z-R coords to P-SV
        do i=1,nscan
          alq3=cs*a(i,3)+sn*a(i,1)
	  alq1=cs*a(i,1)-sn*a(i,3)
	  a(i,3)=alq3
	  a(i,1)=alq1
        end do
      endif
c  first: compute spectrum of pre-event noise
      nst(3)=pstart/dt
      npts0=min0(nst(3)+1,npts)
      call taper(npts0,nwin,el)
      print *,'eigenvalues ',(el(k),k=1,nwin)
      do kcmp=1,3
        do i=1,nf
          sss(i,kcmp)=0.
        end do
        do k=1,nwin
          do i=1,npts0
c OK to reverse order of data here, as we only use mod-sq spectrum
            ar(i)=a(nst(3)+1-i,kcmp)*tap(i,k)
            ai(i)=0.d0
          end do
          do i=npts0+1,npad
            ar(i)=0.d0
            ai(i)=0.d0
          end do   
c  we use the complex FFT routine fft2 -- uses real*8 input arrays
c      print *,'we use the complex FFT routine fft2'
          call fft2(ar,ai,npad)
          do i=1,nf
            sss(i,kcmp)=sss(i,kcmp)+(ar(i)**2+ai(i)**2)
          end do
        end do
c  if pre-event noise duration is truncated, boost the noise spectrum by
c  relative length factor
        if(npts0.lt.npts) then
          do i=1,nf
            sss(i,kcmp)=sss(i,kcmp)*float(npts)/float(npts0)
          end do
        endif
      end do
      if(npts0.lt.npts) then
        call taper(npts,nwin,el)
        print *,'eigenvalues ',(el(k),k=1,nwin)
      endif
      do ips=1,3
        nst(3)=pstart/dt
        nst(1)=(pstart+tsh(ips,irec))/dt
        nst(2)=(pstart+tsh(ips,irec))/dt
c  loop over components
c        print *,'npad,nf,nst,npts',npad,nf,(nst(i),i=1,3),npts
c        print *,'dt,nscan,ddf',dt,nscan,ddf
c      pause
        do kcmp=1,3
c          print *,ips,kcmp,' ips,kcmp'
          do i=1,nf
            sss(i,kcmp+3)=0.
          end do
c  first: compute spectrum of P coda
          do k=1,nwin
            do i=1,npts
              ar(i)=a(i+nst(kcmp),kcmp)*tap(i,k)
              bb(i)=a(i+nst(kcmp),kcmp)*tap(i,k)
              ai(i)=0.d0
            end do
c          call plotit(tt,bb,dum,npts,'tapered data',
c     x     'time',' ',1,0,0.0,0,1)
c            print *,ips,kcmp,k,' ips,kcmp,k'
            do i=npts+1,npad
              ar(i)=0.d0
              ai(i)=0.d0
            end do
c  we use the complex FFT routine fft2 -- uses real*8 input arrays
c      print *,'we use the complex FFT routine fft2'
            call fft2(ar,ai,npad)
c            print *,ips,kcmp,k,' ips,kcmp,k'
            do i=1,nf
              afft(i,k,kcmp)=dcmplx(ar(i),ai(i))
              sss(i,kcmp+3)=sss(i,kcmp+3)+(ar(i)**2+ai(i)**2)
            end do
          end do
c          print *,ips,kcmp,' ips,kcmp'
        end do
c  simple correlation coefficient with vertical (kcmp=3)
c  rf is complex-valued receiver function, damping coefficient is spectrum of pre-event vertical
c  crf is coherence
c  drf is uncertainty estimate
c  crft is the sum of coherence over all records
        do l=1,2
c          print *,ips,l,' ips,l'
	  lps=(ips-1)*2+l
          do n=1,nf
            zc=zero
            do k=1,nwin
              zc=zc+conjg(afft(n,k,3))*afft(n,k,l)
            end do
            rf(n,l)=zc/(sss(n,6)+sss(n,3))
            crf(n,l)=cabs(zc)**2/(sss(n,6)*sss(n,3+l))
            drf(n,l)=sqrt((1.0-crf(n,l))/
     x                 (crf(n,l)*(nwin-1)))*cabs(rf(n,l))
            crft(n,l)=crft(n,l)+crf(n,l)
c            print *,n,l,zc,aaa,rf(n,l)
            rfs(n,irec,lps)=rf(n,l)
            drfs(n,irec,lps)=drf(n,l)
          end do
        end do
      end do
 1076 format(3f10.4)
 1077 format(2f10.4)
 1022 format(f9.5,f6.1,f12.5)
  102 format(a)
      go to 10
  111 nrec=irec
      nftot=nf
      fmaxmax=fmax
      close(10)
c  average coherence as function of frequency
      do l=1,2
        do i=1,nf
          crft(i,l)=crft(i,l)/(3.0*nrec)
        end do
      end do
      print *,irec
      print *,ddf,nf,fmax,npad,dt,(fr(i),i=1,3)
      print *,'(crft(i,1),i=1,10)',(crft(i,1),i=1,10)
      print *,'(crft(i,2),i=1,10)',(crft(i,2),i=1,10)
c      call plotit_axes(0.0,0.0,0.0,1.0)
      bb(1)=0.
      bb(2)=fmax
      bb(3)=1./float(nwin)
      bb(4)=bb(3)
c      call plotit(bb,bb(3),dum,2,' ',' ',' ',2,0,0.0,0,0)
c      call plotit(fr,crft(1,2),dum,nf,'Average P-Coda Coherence',
c     x    'Freq(Hz)','Avg C\sup{2}(f)',1,0,0.05,0,0)
c      call plotit(fr,crft(1,1),dum,nf,'Average P-Coda Coherence',
c     x    'Freq(Hz)','Avg C\sup{2}(f)',1,0,0.0,0,1)
c      call plotit_axes(0.0,0.0,0.0,0.0)
      print *,irec
c  we loop over a grid-search in model-parameter space
c  the parameters are layer thickness and Vp/Vs ratio
c  for this exercise the starting values are H=30km and Vp/Vs approx 1.75.  
c  we search for values 22.le.H.le.38 and 1.5.le.Vp/Vs.le.2.1
c      hh1=32000.
c      hh2=48000.
      hh1=32000.
      hh2=42000.
      hh1=(hmoho-5.)*1000.
      hh2=(hmoho+5.)*1000.
      vpvs1=1.5
      vpvs2=2.1
      do i=1,21
        vpvs(i)=(vpvs1*(21-i)+vpvs2*(i-1))/20.
	hh(i)=(hh1*(21-i)+hh2*(i-1))/20.
      end do
      vpvs0=vp(1)/vs(1)
      hh0=dz(1)
      pfac(1)=1.0
      pfac(2)=0.8
      pfac(3)=-0.8
c      pfac(2)=1.0
c      pfac(3)=-1.0

 1008 format('hkstack with fmax=',f4.1)
      do ifmax=1,6
        fmax=ifmax*0.5
        nf=fmax/ddf+1
	write(string,1008) fmax
	do n=1,nf
	  facc(n)=cos(pih*(n-1)/nf)**2
c	  facc(n)=1.0
	end do
        ivmax=1
	ihmax=1
	hkmax=0.0      
      open(10,file='hkstak.dat',form='formatted')
      do ivpvs=1,21
	fac=sqrt(vpvs(ivpvs)/vpvs0)
	vp1=vp(1)*fac
	vs1=vs(1)/fac
        do ihh=1,21
	  hkstak(ivpvs,ihh)=0.
	  xstak(ivpvs,ihh)=hh(ihh)/1000.
	  ystak(ivpvs,ihh)=vpvs(ivpvs)
c  loop over records
	  do ips=1,3
            do l=1,2
	      lps=(ips-1)*2+l
              do n=1,nf
                rf(n,lps)=zero
                drf(n,lps)=0.
	      end do
            end do 
	  end do   
	  do irec=1,nrec
c  get p-slowness -- iflag branches on the start time of the analysis window
            slow=pslow(epic(irec),iflag(irec))
c  the stack-model velocities are in SI units, s_p is in sec/km
            slw=(slow/1000.)       
            coss=sqrt(1.-vs1*vs1*slw*slw)
            cosp=sqrt(1.-vp1*vp1*slw*slw)
c  trapdoor to avoid incoming slownesses that are evanescent (e.g. head waves)
c  basically, one stacks with the last acceptable interval velocity
            tsh1=hh(ihh)*(coss/vs1-cosp/vp1)
            tsh2=hh(ihh)*(coss/vs1+cosp/vp1)
            tsh3=hh(ihh)*2.0*(coss/vs1)
	    dtsh(1)=tsh1-tsh(1,irec)
	    dtsh(2)=tsh2-tsh(2,irec)
            dtsh(3)=tsh3-tsh(3,irec)
	    do ips=1,3
	      zc=cexp(-eye*pi2*ddf*dtsh(ips))
c	      zc=cexp(eye*pi2*ddf*dtsh(ips))
	      zramp(1)=zone
	      do n=2,nf
	        zramp(n)=zc*zramp(n-1)
	      end do
              do l=1,2
	        lps=(ips-1)*2+l
                do n=1,nf
                  sig=drfs(n,irec,lps)**2
		  if(invwgt.eq.0) sig=1.0
                  rf(n,lps)=rf(n,lps)+zramp(n)*rfs(n,irec,lps)/sig
                  drf(n,lps)=drf(n,lps)+1.0/sig
                end do  
              end do
	    end do
	  end do
c  invert the FT to obtain the RF, but we only want the t=0 part of the radial RF
c  which is just a raw sum of the real-part H(f)
          do ips=1,3
	    lps=(ips-1)*2+1
	    pulse(ips)=0.
            do n=1,nf
c              fac=cos(pih*(n-1)/nf)**2
              rf(n,lps)=rf(n,lps)/drf(n,lps)
              pulse(ips)=pulse(ips)+facc(n)*real(rf(n,lps))
            end do
	    hkstak(ivpvs,ihh)=hkstak(ivpvs,ihh)+pfac(ips)*pulse(ips)
	  end do
	  print *,xstak(ivpvs,ihh),ystak(ivpvs,ihh),hkstak(ivpvs,ihh)
      write(10,109) xstak(ivpvs,ihh),ystak(ivpvs,ihh),hkstak(ivpvs,ihh)
	  if(hkstak(ivpvs,ihh).gt.hkmax) then
	    ivmax=ivpvs
	    ihmax=ihh
	    hkmax=hkstak(ivpvs,ihh)
	  endif
        end do
      end do
  109 format(3g15.5)
      close(10)
      print *,'hkmax depth, Vp/Vs and amplitude',xstak(ivmax,ihmax),
     x   ystak(ivmax,ihmax),hkstak(ivmax,ihmax)
c      call plotit(xstak,ystak,hkstak,441,string,
c     x 'Layer thickness (km)','Vp/Vs ratio',4,0,0.3,6,1)        
      
c      call plotit(xstak,ystak,hkstak,441,string,
c     x 'Layer thickness (km)','Vp/Vs ratio',5,0,0.3,6,1)        
      h1=hh1/1000.
      h2=hh2/1000.
c      call contour(hkstak,21,21,string,
c     x 'Layer thickness (km)','Vp/Vs ratio',1.0,h1,h2,vpvs1,vpvs2)
c      do ivpvs=1,21
c        do ihh=1,21
c	  hkstak(ivpvs,ihh)=(ivpvs-10.0)**2-ihh*5.0
c        end do
c      end do
c      call contour(hkstak,21,21,'test array',
c     x 'Layer thickness (km)','Vp/Vs ratio',1.0,h1,h2,vpvs1,vpvs2)
	  
	  
      end do
      
      stop
      end
c
      subroutine taper(n,nwin,el)
c
c  to generate slepian tapers
c  ta is a real*4 array
c
c         j. park
c
      real*8 el,a,z,pi,ww,cs,ai,an,eps,rlu,rlb
      real*8 dfac,drat,gamma,bh,ell
      common/nnaamm/iwflag
      common/npiprol/anpi
      common/tapsum/tapsum(20),ell(20)
      common/work/ip(16400)        
      common/taperzz/z(263000)  ! we use this common block for scratch space
      common/stap2/ta(16400,8)
      dimension a(16400,8),el(10)
      data iwflag/0/,pi/3.14159265358979d0/
      equivalence (a(1,1),ta(1,1))
      an=dfloat(n)
      ww=dble(anpi)/an
      cs=dcos(2.d0*pi*ww)
c initialize matrix for eispack subroutine
c      print *,'ww,cs,an',ww,cs,an
      do i=0,n-1
        ai=dfloat(i)
        a(i+1,1)=-cs*((an-1.d0)/2.d0-ai)**2
        a(i+1,2)=-ai*(an-ai)/2.d0
        a(i+1,3)=a(i+1,2)**2        ! required by eispack routine
      end do
      eps=1.e-13
      m11=1
      call tridib(n,eps,a(1,1),a(1,2),a(1,3),rlb,rlu,m11,nwin,el,ip,
     x       ierr,a(1,4),a(1,5))
c      print *,ierr,rlb,rlu
      print *,'eigenvalues for the eigentapers'
c      print *,(el(i),i=1,nwin)
      call tinvit(n,n,a(1,1),a(1,2),a(1,3),nwin,el,ip,z,ierr,
     x            a(1,4),a(1,5),a(1,6),a(1,7),a(1,8))      
c  we calculate the eigenvalues of the dirichlet-kernel problem
c  i.e. the bandwidth retention factors
c  from slepian 1978 asymptotic formula, gotten from thomson 1982 eq 2.5
c  supplemented by the asymptotic formula for k near 2n from slepian 1978 eq 61
      dfac=an*pi*ww
      drat=8.d0*dfac
      dfac=4.d0*dsqrt(pi*dfac)*dexp(-2.d0*dfac)
      do k=1,nwin
        el(k)=1.d0-dfac
        dfac=dfac*drat/k  ! is this correct formula? yes,but fails as k -> 2n
      end do
c      print *,'eigenvalues for the eigentapers (small k)'
c      print *,(el(i),i=1,nwin)
      gamma=dlog(8.d0*an*dsin(2.d0*pi*ww))+0.5772156649d0
      do k=1,nwin
        bh=-2.d0*pi*(an*ww-dfloat(k-1)/2.d0-.25d0)/gamma
        ell(k)=1.d0/(1.d0+dexp(pi*bh))
      end do
      do i=1,nwin
        el(i)=dmax1(ell(i),el(i))
      end do     
c   normalize the eigentapers to preserve power for a white process
c   i.e. they have rms value unity
      do k=1,nwin
        kk=(k-1)*n
        tapsum(k)=0.
        tapsq=0.
        do i=1,n
          aa=z(kk+i)
          ta(i,k)=aa
          tapsum(k)=tapsum(k)+aa
          tapsq=tapsq+aa*aa
        end do
        aa=sqrt(tapsq/n)
        tapsum(k)=tapsum(k)/aa
        do i=1,n
          ta(i,k)=ta(i,k)/aa
        end do
      end do
c      print *,'tapsum',(tapsum(i),i=1,nwin)
  101 format(80a)
c  refft preserves amplitudes with zeropadding
c  zum beispiel: a(i)=1.,i=1,100 will transform at zero freq to b(f=0.)=100
c  no matter how much zero padding is done
c  therefore we need not doctor the taper normalization,
c  but wait until the fft to force the desired units
      iwflag=1
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c               routine
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fft2(ar,ai,n)
c  fft routine with 2 real input arrays rather than complex - see ffttwo
c  for comments. 
c  fft2 subroutine mults by exp(i\omega t)
c  OUTPUT: f=0 in ar(1), f=f_N in ar(n/2+1)
c          ar(i)=ar(n+2-i), ai(i)=-ai(n+2-i)
c  fft2 is NOT a unitary transform, mults the series by sqrt(n)
c  the inverse FT can be effected by running fft2 on the conjugate of
c  the FFT-expansion, then taking the the conjugate of the output, 
c  and dividing thru by N. to wit:
c
c   assume Xr, Xi is in freq domain, xr, xi in the time domain
c
c   (Xr,Xi)=fft2(xr,xi,N)
c   (xr,-xi)=fft2(Xr,-Xi,N)/N
c
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      dimension ar(1),ai(1)
      mex=dlog(dble(float(n)))/.693147d0
      nv2=n/2
      nm1=n-1
      j=1
      do 7 i=1,nm1
      if(i .ge. j) go to 5
      tr=ar(j)
      ti=ai(j)
      ar(j)=ar(i)
      ai(j)=ai(i)
      ar(i)=tr
      ai(i)=ti
   5  k=nv2
   6  if(k .ge. j) go to 7
      j=j-k
      k=k/2
      go to 6
   7  j=j+k
      pi=3.14159265358979d0
      do 20 l=1,mex
      le=2**l
      le1=le/2
      ur=1.0
      ui=0.
      wr=dcos(pi/le1 )
      wi=dsin (pi/le1)
      do 20 j=1,le1
      do 10 i=j,n,le
      ip=i+le1
      tr=ar(ip)*ur - ai(ip)*ui
      ti=ai(ip)*ur + ar(ip)*ui
      ar(ip)=ar(i)-tr
      ai(ip)=ai(i) - ti
      ar(i)=ar(i)+tr
      ai(i)=ai(i)+ti
  10  continue
      utemp=ur
      ur=ur*wr - ui*wi
      ui=ui*wr + utemp*wi
  20  continue
      return
      end
      subroutine read_ttimes
      implicit real*4(a-h,o-z)
      implicit integer*4(i-n)
      common/tt_p/d_p(200),d_pkp(200),s_p(200),s_pkp(200),n_p,n_pkp
c  read in the P- and PKP-slowness values from ttimes
c  the code interpolates across the upper-mantle triplication for DELTA=19-27
c  and chooses P or PKP based on the start time of the data-analysis window
c  for DELTA.ge.105
      do i=1,200
        d_p(i)=0.
      end do
c  fill in the head-wave slowness for DELTA.lt.14
c  13.2 sec/degree is 8.4+eps km/sec
      do i=1,13
        d_p(i)=float(i)
        s_p(i)=13.2
      end do
c      open(7,file='/park/backup/Vary/Layers/ttimes_P.dat',
      open(7,file='/Users/jjpark/Research/Layers/ttimes_P.dat',
     x  form='formatted')
      do i=14,200
        read(7,*,end=110) d_p(i),s_p(i)
      end do
  110 close(7)
      print *,i,' = counter at end of ttimes file'
      do i=1,200
        if(d_p(i).eq.0.) go to 120
      end do
  120 n_p=i-1
c  interpolates the triplication
c  linearly interpolate slowness between Delta=17 and p=11.06 sec/degree
c                                    and Delta=28 and p=8.92 sec/degree
      do i=18,27
        d_p(i)=float(i)
        s_p(i)=11.06-(i-17)*(11.06-8.92)/11.
      end do
      do i=45,n_p
        d_p(i-18)=d_p(i)
        s_p(i-18)=s_p(i)
      end do
      n_p=n_p-18
      do i=1,200
        d_pkp(i)=0.
      end do
c      open(7,file='/park/backup/Vary/Layers/ttimes_PKP.dat',
      open(7,file='/Users/jjpark/Research/Layers/ttimes_PKP.dat',
     x  form='formatted')
      do i=1,200
        read(7,*,end=130) d_pkp(i),s_pkp(i)
      end do
  130 close(7)
      print *,i,' = counter at end of ttimes file'
      do i=1,200
        if(d_pkp(i).eq.0.) go to 140
      end do
  140 n_pkp=i-1
      factor=180./(3.14159265358979*6371.)
      do i=1,n_p
        s_p(i)=s_p(i)*factor
      end do
      do i=1,n_pkp
        s_pkp(i)=s_pkp(i)*factor
      end do
      print *,'n_p,n_pkp',n_p,n_pkp
c      call plotit_axes(0.,0.,0.,0.)
c      call plotit(d_p,s_p,dum,n_p,' ',' ',' ',2,0,0.0,0,0)
c      call plotit(d_pkp,s_pkp,dum,n_pkp,' ',
c     x 'Epicentral Distance (degrees)','Slowness (sec/km)',2,0,0.0,0,1)
      return
      end
      subroutine write_ttimes
      implicit real*4(a-h,o-z)
      implicit integer*4(i-n)
      common/tt_p/d_p(200),d_pkp(200),s_p(200),s_pkp(200),n_p,n_pkp
c  read in the P- and PKP-slowness values from ttimes
c  the code interpolates across the upper-mantle triplication for DELTA=19-27
c  and chooses P or PKP based on the start time of the data-analysis window
c  for DELTA.ge.105
      do i=1,200
        d_p(i)=0.
      end do
c  fill in the head-wave slowness for DELTA.lt.14
c  13.2 sec/degree is 8.4+eps km/sec
      do i=1,13
        d_p(i)=float(i)
        s_p(i)=13.2
      end do
c      open(7,file='/park/backup/Vary/Layers/ttimes_P.dat',
      open(7,file='/Users/jjpark/Research/Layers/ttimes_P.dat',
     x  form='formatted')
      do i=14,200
        read(7,*,end=110) d_p(i),s_p(i)
      end do
  110 close(7)
      print *,i,' = counter at end of ttimes file'
      do i=1,200
        if(d_p(i).eq.0.) go to 120
      end do
  120 n_p=i-1
c  interpolates the triplication
c  linearly interpolate slowness between Delta=17 and p=11.06 sec/degree
c                                    and Delta=28 and p=8.92 sec/degree
      do i=18,27
        d_p(i)=float(i)
        s_p(i)=11.06-(i-17)*(11.06-8.92)/11.
      end do
      do i=45,n_p
        d_p(i-18)=d_p(i)
        s_p(i-18)=s_p(i)
      end do
      n_p=n_p-18
      write(6,'(a)') '      data d_p/'
      write(6,1001) (d_p(i),i=1,n_p)
      write(6,'(a)') '     x  /'
 1001 format(5x,'x',1x,8(f5.1,','))
      write(6,'(a)') '      data s_p/'
      write(6,1002) (s_p(i),i=1,n_p)
      write(6,'(a)') '     x  /'
 1002 format(5x,'x',1x,8(f7.4,','))
      do i=1,200
        d_pkp(i)=0.
      end do
c      open(7,file='/park/backup/Vary/Layers/ttimes_PKP.dat',
      open(7,file='/Users/jjpark/Research/Layers/ttimes_PKP.dat',
     x  form='formatted')
      do i=1,200
        read(7,*,end=130) d_pkp(i),s_pkp(i)
      end do
  130 close(7)
      print *,i,' = counter at end of ttimes file'
      do i=1,200
        if(d_pkp(i).eq.0.) go to 140
      end do
  140 n_pkp=i-1
      write(6,'(a)') '      data d_pkp/'
      write(6,1001) (d_pkp(i),i=1,n_pkp)
      write(6,'(a)') '     x  /'
      write(6,'(a)') '      data s_pkp/'
      write(6,1002) (s_pkp(i),i=1,n_pkp)
      write(6,'(a)') '     x  /'
      factor=180./(3.14159265358979*6371.)
      do i=1,n_p
        s_p(i)=s_p(i)*factor
      end do
      do i=1,n_pkp
        s_pkp(i)=s_pkp(i)*factor
      end do
      print *,'n_p,n_pkp',n_p,n_pkp
c      call plotit_axes(0.,0.,0.,0.)
c      call plotit(d_p,s_p,dum,n_p,' ',' ',' ',2,0,0.0,0,0)
c      call plotit(d_pkp,s_pkp,dum,n_pkp,' ',
c     x 'Epicentral Distance (degrees)','Slowness (sec/km)',2,0,0.0,0,1)
      return
      end
      subroutine fill_ttimes
      implicit real*4(a-h,o-z)
      implicit integer*4(i-n)
      common/tt_p/d_p(200),d_pkp(200),s_p(200),s_pkp(200),n_p,n_pkp
      data d_p/
     x   1.0,  2.0,  3.0,  4.0,  5.0,  6.0,  7.0,  8.0,
     x   9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0,
     x  17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0,
     x  25.0, 26.0, 27.0, 28.0, 29.0, 30.0, 31.0, 32.0,
     x  33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0,
     x  41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0,
     x  49.0, 50.0, 51.0, 52.0, 53.0, 54.0, 55.0, 56.0,
     x  57.0, 58.0, 59.0, 60.0, 61.0, 62.0, 63.0, 64.0,
     x  65.0, 66.0, 67.0, 68.0, 69.0, 70.0, 71.0, 72.0,
     x  73.0, 74.0, 75.0, 76.0, 77.0, 78.0, 79.0, 80.0,
     x  81.0, 82.0, 83.0, 84.0, 85.0, 86.0, 87.0, 88.0,
     x  89.0, 90.0, 91.0, 92.0, 93.0, 94.0, 95.0, 96.0,
     x  97.0, 98.0, 99.0,100.0,101.0,102.0,103.0,104.0,
     x 105.0,106.0,107.0,108.0,109.0,110.0,111.0,112.0,
     x 113.0,114.0,115.0,116.0,117.0,118.0,119.0,120.0,
     x 121.0,122.0,123.0,124.0,125.0,126.0,127.0,128.0,
     x 129.0,130.0,70*0.0/
      data s_p/
     x 13.2000,13.2000,13.2000,13.2000,13.2000,13.2000,13.2000,13.2000,
     x 13.2000,13.2000,13.2000,13.2000,13.2000,11.1152,11.1094,11.0931,
     x 11.0643,10.8655,10.6709,10.4764,10.2818,10.0873, 9.8927, 9.6982,
     x  9.5036, 9.3091, 8.9897, 8.9231, 8.8777, 8.8398, 8.8035, 8.7632,
     x  8.7141, 8.6616, 8.6068, 8.5484, 8.4879, 8.4257, 8.3610, 8.2939,
     x  8.2271, 8.1596, 8.0906, 8.0209, 7.9507, 7.8798, 7.8085, 7.7371,
     x  7.6651, 7.5926, 7.5201, 7.4481, 7.3761, 7.3038, 7.2314, 7.1589,
     x  7.0864, 7.0141, 6.9416, 6.8692, 6.7967, 6.7242, 6.6516, 6.5789,
     x  6.5062, 6.4334, 6.3607, 6.2880, 6.2151, 6.1420, 6.0688, 5.9953,
     x  5.9217, 5.8477, 5.7733, 5.6987, 5.6239, 5.5489, 5.4732, 5.3969,
     x  5.3204, 5.2439, 5.1660, 5.0853, 5.0069, 4.9354, 4.8509, 4.7347,
     x  4.6911, 4.6601, 4.6347, 4.6132, 4.5944, 4.5742, 4.5475, 4.5165,
     x  4.4847, 4.4504, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389,
     x  4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389,
     x  4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389,
     x  4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389, 4.4389,
     x  4.4389, 4.4389,70*0.0/
      data d_pkp/
     x  90.0, 91.0, 92.0, 93.0, 94.0, 95.0, 96.0, 97.0,
     x  98.0, 99.0,100.0,101.0,102.0,103.0,104.0,105.0,
     x 106.0,107.0,108.0,109.0,110.0,111.0,112.0,113.0,
     x 114.0,115.0,116.0,117.0,118.0,119.0,120.0,121.0,
     x 122.0,123.0,124.0,125.0,126.0,127.0,128.0,129.0,
     x 130.0,131.0,132.0,133.0,134.0,135.0,136.0,137.0,
     x 138.0,139.0,140.0,141.0,142.0,143.0,144.0,145.0,
     x 146.0,147.0,148.0,149.0,150.0,151.0,152.0,153.0,
     x 154.0,155.0,156.0,157.0,158.0,159.0,160.0,161.0,
     x 162.0,163.0,164.0,165.0,166.0,167.0,168.0,169.0,
     x 170.0,171.0,172.0,173.0,174.0,175.0,176.0,177.0,
     x 178.0,179.0,180.0,109*0.0/
      data s_pkp/
     x  1.6815, 1.6935, 1.7053, 1.7170, 1.7285, 1.7397, 1.7508, 1.7617,
     x  1.7724, 1.7830, 1.7933, 1.8034, 1.8134, 1.8231, 1.8327, 1.8421,
     x  1.8513, 1.8603, 1.8691, 1.8777, 1.8861, 1.8943, 1.9024, 1.9102,
     x  1.9178, 1.9253, 1.9326, 1.9361, 1.9360, 1.9356, 1.9351, 1.9344,
     x  1.9335, 1.9323, 1.9309, 1.9292, 1.9272, 1.9249, 1.9222, 1.9191,
     x  1.9155, 1.9115, 1.9069, 1.9017, 1.8959, 1.8893, 1.8819, 1.8735,
     x  1.8641, 1.8536, 1.8418, 1.8286, 1.8138, 2.2973, 2.7791, 3.2678,
     x  3.0608, 2.9096, 2.7819, 2.6680, 2.5632, 2.4648, 2.3712, 2.2808,
     x  2.1961, 2.1105, 1.3849, 1.3377, 1.2889, 1.2384, 1.1865, 1.1333,
     x  1.0790, 1.0235, 0.9672, 0.9100, 0.8520, 0.7935, 0.7343, 0.6746,
     x  0.6145, 0.5541, 0.4933, 0.4322, 0.3708, 0.3093, 0.2477, 0.1858,
     x  0.1240, 0.0621, 0.0000,109*0.0/
c  read in the P- and PKP-slowness values from ttimes
c  the code interpolates across the upper-mantle triplication for DELTA=19-27
c  and chooses P or PKP based on the start time of the data-analysis window
c  for DELTA.ge.105
c  fill in the head-wave slowness for DELTA.lt.14
c  13.2 sec/degree is 8.4+eps km/sec
c      open(7,file='/Users/jjpark/Research/Layers/ttimes_P.dat',form='formatted')
      n_p=130
c  interpolates the triplication
c  linearly interpolate slowness between Delta=17 and p=11.06 sec/degree
c                                    and Delta=28 and p=8.92 sec/degree
      n_pkp=91
      factor=180./(3.14159265358979*6371.)
      do i=1,n_p
        s_p(i)=s_p(i)*factor
      end do
      do i=1,n_pkp
        s_pkp(i)=s_pkp(i)*factor
      end do
      print *,'n_p,n_pkp',n_p,n_pkp
c      call plotit_axes(0.,0.,0.,0.)
c      call plotit(d_p,s_p,dum,n_p,' ',' ',' ',2,0,0.0,0,0)
c      call plotit(d_pkp,s_pkp,dum,n_pkp,' ',
c     x 'Epicentral Distance (degrees)','Slowness (sec/km)',2,0,0.0,0,1)
      return
      end
c
      subroutine pause(string)
      character*(*) string
      print *,string
      read(5,*) 
      return
      end
c
      real*4 function pslow(epic,iflag)
      implicit real*4(a-h,o-z)
      implicit integer*4(i-n)
      common/tt_p/d_p(200),d_pkp(200),s_p(200),s_pkp(200),n_p,n_pkp
      igcarc=epic
      fac=epic-igcarc
c      print *,'igcarc,i,epic,fac',igcarc,irec,epic,fac
      if(iflag.eq.1) then		! P-phase
        if(igcarc.ge.129.or.igcarc.le.0) then
          print *,'WHOA! igcarc,gcarc = ',igcarc,epic
c          pause
        endif
        slow=(1.-fac)*s_p(igcarc)+fac*s_p(igcarc+1)
      elseif(iflag.eq.2) then	! PKP-phase
        if(igcarc.ge.180.or.igcarc.lt.90) then
          print *,'WHOA! igcarc,gcarc = ',igcarc,epic
c          pause
        endif
        ig=igcarc-89
        slow=(1.-fac)*s_pkp(ig)+fac*s_pkp(ig+1)
      else
        print *,'WHOA! iflag(i),i = ',iflag,irec
      endif
      pslow=slow
      return
      end
