c  program sac_wavelet
c  JJP 11/12/97
c  f77 -o /park/backup/bin/sac_wavelet sac_wavelet.f /park/backup/Ritz/eislib.a /park/backup/Ritz/jlib.a
c  xf77 -o /park/backup/bin/sac_wavelet sac_wavelet.f /park/backup/Plotxy/plotlib.a /park/backup/Ritz/eislib.a /park/backup/Ritz/jlib.a
c  f77 -o /Users/jjpark/bin/sac_wavelet sac_wavelet.f /Users/jjpark/Ritz/eislib.a /Users/jjpark/Ritz/jlib.a
c  xf77 -o /Users/jjpark/bin/sac_wavelet sac_wavelet.f /Users/jjpark/Plotxy/plotlib.a /Users/jjpark/Ritz/eislib.a /Users/jjpark/Ritz/jlib.a
c
c  to read SAC-format data, filename.bh? <-- routine subs e,n,z for "?"
c
c  generates wavelets with eigenvector decomposition + interpolation
c  calculate padded FFT of data for convolution with wavelet FFTs
c  general use - calculates set of wavelets for a specified number of points
c  interpolates and fft other lengths on the fly
c  max=12 real-wavelets max = 6 complex-valued wavelets
c
c  code is based on the algorithms described in 
c
c  Lilly, J., and J. Park, Multiwavelet spectral and polarization analysis 
c  of seismic records, Geophysical J. International, v122, 1001-1021, 1995.
c
c  Park, J. and M. E. Mann, Interannual temperature events and shifts in 
c  global temperature: A multiwavelet correlation approach, Earth Interactions, 
c  v. 4, online paper 4-001, 2000.
c
C  SEE ALSO:
c  Bear, L. K., and G. L. Pavlis, Estimation of slowness vectors and
c  their uncertainties using multi-wavelet seismic array processing, 
c  Bull. Seismol. Soc. Am., v87, 755--769, 1997.
c
c  this code generates 2-D arrays of spectra, polarization and statistical stuff
c  it writes GMT scripts to display these arrays in dazzling color
c
c  for an input filename sacfile.bh?  the GMT scripts are written to
c  GMT_sacfile.bh, GM[123]_sacfile.bh
c
c  the tarfile for distribution should have binary SACfiles nilcut.sh? 
c  -- a P wave coda
c  you can test the program on this data
c  
c  the ASCII files to plot are named 
c  cpt_nilcut.sh   lam_nilcut.sh1  spw_nilcut.shz
c  dat_nilcut.sh   lam_nilcut.sh2  spw_nilcut.she
c  ell_nilcut.sht  lam_nilcut.sh3  spw_nilcut.shn
c  ell_nilcut.shz  spw_nilcut.shs
c  cp2_nilcut.sh   lam_nilcut.sh0  spw_nilcut.shv
c
c  dat_* is data to plot
c  ell_* contains polarization ellipses in Z/R (z) and T/R (t) planes
c  spw_* are wavelet spectra fot *z *r *t
c  spw_*s contains splitting ellipses and spw_*v has rectilinearity estimate
c  cpt_* and cp2_* are colorfiles for GMT
c  lam_* are singular values of the wavelet polariation matrices
c
c  you can fill a lot of disk space if you are not tidy with old files!
c
c  user must make scripts executable ("chmod +x GM*_sacfile.bh"),
c  execute the scripts and display the postscript files
c
      implicit real*4 (a-h,o-z)
      character*80 name,nami,title,namo
      character*8 chan
      character*4 chead
      character*4 ista
      character comp(3)
      real*8 ar,ai,br,bi,wr,wi,p,pc
      complex*16 afft(131100),wfft(131100),cmpy
c  we read in data in scratch space
      common/sing2/a(80000,3)
      common/dataser/x4(131100),y4(131100),ys4(131100)
      common/ffts/ar(131100),ai(131100),br(131100),bi(131100),
     x            wr(131100),wi(131100)
      common/wavelets/wav(200000),ip(50),swav(200000),wwav(200000)
      common/mwtransf/wavr(24000,3,20,3),wavi(24000,3,20,3)
      common/udhedz/iy,id,ih,im,iss,ims,dt,ista,chan,nscan
      common/header/ahead(158)
      common/idiot/qcyc,rfrq,ftfrq,fr(3),k0
      common/gmtplot/x9(1600000),y9(1600000),z9(3200000)
      common/ordplot/xq(500),yq(500)
      data comp/'Z','N','E'/
      dimension time(3),tt(3)
      dimension head(35),dhead(7),iah(158),chead(158)
      equivalence (iy,head),(d0,dhead),(ahead,iah),(ahead,chead)
  101 format(a)
      pi=3.14159265358979
      pi2=2.*pi
      rdi=360./pi2
      nskip=6
c  first, create the slepian wavelets, 
c  read in time-bandwidth product and time-bandcenter product
c  e.g. p=2.5, pc=4.0
      print *,'e.g. p=2.5, pc=4.0, nwav=6 real wavelets, nwpts=200'
  10  continue
      nwpts=200
      p=2.5
      pc=5.0
      nwav=6
      nwh=nwav/2
      print *,'p,pc,nwav,nwpts',p,pc,nwav,nwpts
c      read(5,*) p,pc,nwav,nwpts
      if(nwpts.gt.1000) then
        print *,'nwpts too large! max = 1000'
        go to 10
      endif
      if(nwav.gt.12) then
        print *,'nwav too large! max = 12'
        go to 10
      endif
c  XXXXXXXXXXXXXXXXXX subroutine to generate slepian wavelets
      call slepwav(p,pc,nwav,nwpts,wav)
c  spline coefs for the wavelets:
c      print *,'spline coefs for the wavelets:'
      do nw=1,nwav
        nn=nwpts*(nw-1)+1
        call splneq(nwpts,wav(nn),swav(nn))
      end do
      print *,'enter min period (sec), max period, # of wavelet banks'
c      read(5,*) tmin,tmax,nbank
c  hardwired for BHZ 20 sps
c      tmin=0.2
c      tmax=30.
      nbank=16
c  for single 3-comp records
  99  print *,'input seismic data file (<stop>=stop): '
      read(5,101) nami
      if(nami(1:4).eq.'stop'.or.nami(1:4).eq.'stop') go to 999
      kcmp=0
      j9=0
c  we assume that the last character is either 'E','N',or 'Z'
      do i=80,1,-1
        if(nami(i:i).ne.' ') then
          namlen=i
          go to 98
        endif
      end do
  98  kcmp=kcmp+1
      nami(namlen:namlen)=comp(kcmp)
      print *,nami
      call sacread(nami,a(1,kcmp),ierr)
      tt(1)=ahead(1)
      tt(2)=ahead(1)
      tt(3)=ahead(1)
      nscan=iah(80)
      print *,ahead(1),nscan
c new version - we normalize acceleration data series to microns/sec^2
c  and take first differences to boost the high frequencies
c      do i=1,nscan-1
c        a(i,kcmp)=1.e6*(a(i+1,kcmp)-a(i,kcmp))
c      end do
c      a(nscan,kcmp)=0.
c  and take first differences to boost the high frequencies
c
c  scale the data 
c      print *,'scaling data by factor of 1,000,000'
c      do i=1,nscan
c        a(i,kcmp)=1.e6*a(i,kcmp)
c      end do
c  add a random part to perfect synthetics (dont use for real data)
      print *,'adding small random noise to time series'
      do i=1,nscan
        a(i,kcmp)=a(i,kcmp)+1.0e-4*rand(0)
      end do
      call plotit(tt,a(1,kcmp),dum,nscan,'data','time (sec)',
     x 'amplitude',1,0,0,0,1)
c      namo='testfile.dat'
c      call sacout(namo,a(1,kcmp))
      if(ierr.ne.0) go to 99    ! in case of misprintd filename
c      print *,'output file for wavelet transform'
c      read(5,101) name
      dt=ahead(1) 
      baz=ahead(53)
      print *,'back azimuth (CW from N)  ',baz
c  hardwired for half-Nyquist bandpass, with "dynamic range" of 50 or 25
c  for BH? data, plots from 0.125 sec period
      tmin=40.*dt
c  for BH? data, plots from 0.2 sec period
c      tmin=4.*dt
c  for BH? data, plots from 0.5 to 25 sec period
c     tmin=10.*dt
      tmax=50.*tmin
c  for BH? data, plots from 1 to 25 sec period
c      tmin=20.*dt
c      tmax=25.*tmin
      scfac=(tmax/tmin)**(1./float(nbank-1))
c      print *,ahead(32),ahead(33),ahead(36),ahead(37)
c      print *,ahead(51),ahead(52),ahead(53),ahead(54)
c      pause
      ann=pc*tmax/dt
      do i=1,nbank
        ip(i)=ann+0.1
        if(ip(i).ne.2*(ip(i)/2)) ip(i)=ip(i)-1
        ann=ann/scfac
      end do
      dhr=dt/3600.
      iy=iah(71)
      id=iah(72)
      ih=iah(73)
      im=iah(74)
      iss=iah(75)
      ims=iah(76)
      npts=iah(80)
c  restrict to first 1000 sec
c      npts=20000
c  limit, for now, the data file to 8192 points, or to file with fewest points
c      npts=min0(npts,8192)
c      if(dt.gt.0.99) npts=2000  ! limit to body wavetrain
c  demean 
      sum=0.
      do i=1,npts
        sum=sum+a(i,kcmp)
      end do
      sum=sum/npts
      do i=1,npts
        a(i,kcmp)=a(i,kcmp)-sum
      end do
      if(kcmp.lt.3) go to 98
c  set up some parameters
      rfrq=1./(npts*dt)
      fny=1./(2.*dt)
      fzero=0.
      time(1)=0.
      time(2)=dt
      iexpo=alog(float(npts-1))/alog(2.)+1
      npad=2**iexpo
      print *,'npad,npts',npad,npts
c  rotate the horizontals to radial and transverse, 3=east,   2=north
c                                        becomes    3=radial, 2=transverse
      sn=sin(baz/rdi)
      cs=cos(baz/rdi)
      do i=1,npts
        ar(i)=-cs*a(i,2)-sn*a(i,3)  !radial
        ai(i)=-sn*a(i,2)+cs*a(i,3)  !transverse
      end do
      do i=1,npts
        a(i,3)=ar(i)
        a(i,2)=ai(i)
      end do
c  normalize the data to unit variance per data point.
c        sum=0.
c        do i=1,npts
c          sum=sum+a(i,kcmp)**2
c        end do
c        anorm(m)=sqrt(sum/npts)
c  loop over components
      kcmp=0
  198 kcmp=kcmp+1
      do i=1,npts
c new version - we have normalized acceleration data series to microns/sec^2
        ar(i)=a(i,kcmp)
        ai(i)=0.d0
      end do
      do i=npts+1,npad
        ar(i)=0.d0
        ai(i)=0.d0
      end do
c  we use the complex FFT routine fft2
c      print *,'we use the complex FFT routine fft2'
      call fft2(ar,ai,npad)
      do i=1,npad
        afft(i)=dcmplx(ar(i),ai(i))
      end do
c  next: loop over nbank; interpolate wavelet to ip(k) pnts; fft wavelet
c      print *,'mult the conjugate with data; inv-fft'
      do nb=1,nbank
        nip=ip(nb)
        cper=dt*nip/pc
        cperlog=alog10(cper)/alog10(2.0)
        dx=float(nwpts)/float(nip)
c  trick to get interpolator to extrapolate a little past the endpoints
        x0=0.5+0.5*dx
        do i=1,npts
          ys4(i)=0.
        end do
c      print *,'do nw=1,nwav,2'
c  compute wavelets if nip is less than the interpolation transition nwpts
        if(nip.lt.nwpts) call slepwav(p,pc,nwav,nip,wwav)
        do nw=1,nwav,2
c  here we branch on whether nip is greater than the interpolation transition
          if(nip.lt.nwpts) then
            nn=nip*(nw-1)
            do i=1,nip
              br(i)=wwav(nn+i)
              bi(i)=wwav(nn+nip+i)
            end do
          else
            nn=nwpts*(nw-1)+1
            xx=x0
            do i=1,nip
c     print *,'br(i)=evaleq(xx,nwpts,wav(nn),swav(nn),0,1.)'
              br(i)=evaleq(xx,nwpts,wav(nn),swav(nn),0,1.)
              bi(i)=evaleq(xx,nwpts,wav(nn+nwpts),swav(nn+nwpts),0,1.)
              xx=xx+dx
            end do
          endif
          do i=nip+1,npad
            br(i)=0.d0
            bi(i)=0.d0
          end do
c      print *,'call fft2(br,bi,npad)'
          call fft2(br,bi,npad)
c      print *,'done: fft2(br,bi,npad)'
c  the dx factor accounts roughly for the normalization
          do i=1,npad
            wfft(i)=dcmplx(br(i),bi(i))*dx
          end do
c     print *,'we do the inverse FFT via kluge '
c  we do the inverse FFT via kluge 
c  the conjugate of the forward fft of the conjugate of the FFT-product
c  note that conjg of wavelet fft is used
c  save only the first npts, as the wavelet transform of the zeropadding is moot
          do i=1,npad
            cmpy=afft(i)*conjg(wfft(i))
            wr(i)=dreal(cmpy)
            wi(i)=-dimag(cmpy)
          end do
c      print *,'call fft2(wr,wi,npad)'
          call fft2(wr,wi,npad)
c      print *,'done: fft2(wr,wi,npad)'
c  we adjust the start time of the convolution so that the time corresponds 
c  to convolution with a wavelet centered at the `time'
c  the convolution is c_k=conjg(w)_j*a_(k+j)
c  so the center of interval for kth index is k+jpts/2
c  therefore we must wraparound the endpoint of the series 
c  to obtain correlations at start of series
c
c  we also save only the time interval of the data series
c  and discard the zero-padded interval
c  the inverse FT using routine fft2 requires a division by npad
c  there is no further normalization necessary to mimic the convolution
c  later, the wavelet reconstruction will apply a factor of two to account for 
c  positive and negative "frequencies"
          j=0
          iph=nip/2
          do i=1,npts,nskip
            j=j+1
c index shift with wraparound fix
            iip=i-iph
            if(iip.le.0) iip=npad+iip
c            print *,j,nw,iip,iph,nip
            wavr(j,nw/2+1,nb,kcmp)=wr(iip)/npad
            wavi(j,nw/2+1,nb,kcmp)=-wi(iip)/npad
            x4(j)=float(i)*dt
            yyy=(wr(iip)**2+wi(iip)**2)
            ys4(j)=ys4(j)+yyy
            y4(j)=sqrt(yyy)/npad
          end do
          jpts=j
c          call plotit(x4,y4,dum,jpts,'single wavelet',' ',' ',
c     x                                         2,0,0,0,41+nw/2)
        end do
        write(6,109)'# of pnts, center period',nip,cper,' sec'
        write(title,109)'# of pnts, center period',nip,cper,' sec'
        do j=1,jpts
          ys4(j)=2*sqrt(ys4(j))/(npad*nwav)
          z9(j9+j)=ys4(j)
          x9(j9+j)=dt+(j-1)*dt*nskip
          y9(j9+j)=cperlog
        end do
        j9=j9+jpts
c        call plotit(x4,ys4,dum,jpts,title,'time ',' ',
c     x                                              2,0,0,0,44)
      end do
  109 format(a,i6,f8.2,a)
      if(kcmp.lt.3) go to 198
c  jpts = # of time points in wavelet transform
c  j9 = jpts*nbank*3 - total points in min/max ops
c  gmtplot0 writes GMT script to make 4-panel graph, 3 spectra & data
      call gmtplot0(x9,y9,z9,j9,npts,dt,jpts,nbank,nami,namlen)
c  next: estimate shearwave splitting from horizontal components
c  using a rectilinearlity test on the wavelet coefficients
      call splitest_rect(jpts,nbank,nwh,dt,pc,ip,z9,baz)
c  gmtplot1 writes GMT script for 4-panel graph, data + splits + R&T spectra 
c      call gmtplot1(x9,y9,z9,j9,npts,dt,jpts,nbank,nami,namlen)

      print *,'entering polar'
      call polar(jpts,nbank,nwh,z9)
      print *,'leaving polar'
c  gmtplot2 writes GMT script to make 4-panel graph, 3 sing-value spectra & data
      call gmtplot2(x9,y9,z9,j9,npts,dt,jpts,nbank,nami,namlen)
c  gmtplot3 writes GMT script: 1st sing-value spectra, 2 LFV+ellipses & data
      call gmtplot3(x9,y9,z9,j9,npts,dt,jpts,nbank,nami,namlen)
      go to 99
  999 stop
      end      
      subroutine splitest_transv(jpts,nbank,nwh,dt,pc,ip,z9,baz)
c  code to estimate SV/SH splitting using rectilinearity & minimal T comp
c
c  with this in hand
      real*8 pc
      complex*8 za,zu,zv,zz,z0
      common/mwtransf/wavr(24000,3,20,3),wavi(24000,3,20,3)
      common/epars/jink,eang(150,20,2),emaj(150,20,2),emin(150,20,2)
      common/angles/cspsi(200),snpsi(200),var(100),xvar(100)
      common/ordplot/xq(500),yq(500)
      dimension za(3,3),zu(3,3),zv(3,3),w(3)
      dimension z9(jpts,nbank,7),ip(1)
      pi=3.14159265358979
      pi2=2.*pi
      rdi=360./pi2
      if(nwh.ne.3) then
        print *,'you have ',nwh,' wavelets, not 3.  Go on??'
        pause
      endif
      z0=cmplx(0.,0.)
c  OK, how many ellipses?  5.5 x 2.3 inches
c  2.3 inch / 16 is roughly 0.15 inch --> 5.5/0.15 = 37
c  try 0.12in instead -- aim for 50 ellipses across
c      jink=jpts/55+1
c  for now, dont skip
      jink=6
c  create the sines and cosines
      do i=1,94
        ang=(i-1)
        cspsi(i)=cos(ang/rdi)
        snpsi(i)=sin(ang/rdi)
        xvar(i)=ang
      end do
      do nb=1,nbank
        nip=ip(nb)
        cper=dt*nip/pc
        ji=0
        do j=1,jpts
          do nw=1,nwh
            do m=1,3
              za(m,nw)=cmplx(wavr(j,nw,nb,m),wavi(j,nw,nb,m))
            end do
          end do
c  loop over psi coarsely
c  1=vertical  3=radial, 2=transverse
c  1=fast   2=slow
          varmax=0.
          ivar=0
          do i=1,91
c          do i=1,91,3
c  rotate components
            do nw=1,nwh
              zu(1,nw)=cspsi(i)*za(3,nw)+snpsi(i)*za(2,nw)
              zu(2,nw)=-snpsi(i)*za(3,nw)+cspsi(i)*za(2,nw)
            end do
c  compute 2x2 cross correlation matrix
            do jj=1,2
              do k=jj,2
                zv(jj,k)=z0
                do nw=1,nwh
                  zv(jj,k)=zv(jj,k)+conjg(zu(jj,nw))*zu(k,nw)  
                end do
              end do
            end do
c  find Rayleigh product of unrotation vector
            aa=cabs(zv(1,1))
            bb=cabs(zv(2,2))
            cc=cabs(zv(1,2))
        var(i)=aa*cspsi(i)**2+bb*snpsi(i)**2+2.*cc*snpsi(i)*cspsi(i)
c  normalize by trace of cross-correlation matrix
            var(i)=var(i)/(aa+bb)
            if(var(i).gt.varmax) then
              varmax=var(i)
              aas=aa
              bbs=bb
              ccs=cc
              ivar=i
            endif
          end do 
          varmax=0.
c  refine maximum var(i), first, shift ivar if at i=1
          if(ivar.eq.1) ivar=91
          i1=ivar-2
          i2=ivar+2
          do i=i1,i2
c  rotate components
            do nw=1,nwh
              zu(1,nw)=cspsi(i)*za(3,nw)+snpsi(i)*za(2,nw)
              zu(2,nw)=-snpsi(i)*za(3,nw)+cspsi(i)*za(2,nw)
            end do
c  compute 2x2 cross correlation matrix
            do jj=1,2
              do k=jj,2
                zv(jj,k)=z0
                do nw=1,nwh
                  zv(jj,k)=zv(jj,k)+conjg(zu(jj,nw))*zu(k,nw)  
                end do
              end do
            end do
c  find Rayleigh product of unrotation vector
            aa=cabs(zv(1,1))
            bb=cabs(zv(2,2))
            cc=cabs(zv(1,2))
        var(i)=aa*cspsi(i)**2+bb*snpsi(i)**2+2.*cc*snpsi(i)*cspsi(i)
c  normalize by trace of cross-correlation matrix
            var(i)=var(i)/(aa+bb)
            if(var(i).gt.varmax) then
              varmax=var(i)
              ivar=i
              aas=aa
              bbs=bb
              ccs=cc
              zz=zv(1,2)
            endif
          end do 
c  shift ivar back if over 90 degrees
          if(ivar.gt.90) ivar=ivar-90
c  compute the time shift
c  by convention, we would like to keep DT positive, less than one half-cycle
c  the trick is to find angle lambda such that
c  arg(-zz*sin(2\psi)*exp(-2i\lambda)) = 0
c  in the first quadrant, over which we search, sin 2\psi is nonnegative, 
c  and 2\lambda=2\pi(DT/T)= arg(-zz)
c
          are=real(-zz)
          aim=imag(-zz)
          rad=atan2(aim,are)
          delt=rad*cper/pi2
          fast=ivar-1
c  uncertainty estimate
c  we use the fractional residual var (1.-varmax) to estimate the noise level
c  For time delay, the bestfit is the peak of a sinusoid (at DT0)
c  var(i)=aa*cspsi**2+bb*snpsi**2+2.*cc*snpsi*cspsi*COS(2pi*(DT-DT0)/T)
c  if cc is large, the uncertainty in DT is small
c  if cc is small, the uncertainty in DT is large
c  can obtain uncertainty in degrees via an inv cos
          varno=1.0-varmax
          ccvar=abs(2.*cc*snpsi(ivar)*cspsi(ivar))/(aa+bb)
          if(ccvar.gt.0) then
            cser=(ccvar-varno)/ccvar
          else
            cser=-2.
          endif
          if(cser.le.-1.0) then
            ddt=cper/2.
          else
            ddt=cper*acos(cser)/pi2
          endif
c  for uncertainty in angle of fast axis, we compare values of var(i) explicitly
          varfloor=varmax-varno
          do i=ivar,94
            if(var(i).lt.varfloor) go to 123
          end do
  123     ddth1=i-ivar
          do i=ivar,1,-1
            if(var(i).lt.varfloor) go to 234
          end do
  234     ddth2=ivar-i
          ddth=amax1(ddth1,ddth2)
          if(ddth1+ddth2.ge.90.) ddth=90.
c  if delt < 0, then we have found the slow axis, not the fast axis
c  we rotate psi CW by 90 degrees, and flip the sign of delt
          if(delt.lt.0.) then
            delt=-delt
            fast=fast-90.
          endif
c  trapdoor to print out var(i)
c          if(j.eq.jpts/2.and.cper.gt.3.5.and.cper.lt.4.5) then
c            print *,nip,cper,j,fast,delt,varmax
c            print *,zz,rad
c            do nw=1,nwh
c              do m=1,3
c                za(m,nw)=cmplx(wavr(j,nw,nb,m),wavi(j,nw,nb,m))
c              end do
c            end do
c            print *,za(2,1),za(3,1)
c            print *,za(2,2),za(3,2)
c            print *,za(2,3),za(3,3)
c            xq(1)=0.
c            xq(2)=cper/2.
c            yq(1)=varfloor
c            yq(2)=varfloor
c            call plotit(xq,yq,dum,2,' ',' ',' ',2,0,0.1,0,0)
c            do i=1,360
c              xq(i)=float(i)*cper/360.
c              phi=pi2*delt/cper
c              yq(i)=(aas*cspsi(ivar)**2+bbs*snpsi(ivar)**2+
c     x   2.*cc*snpsi(ivar)*cspsi(ivar)*cos(float(i)/rdi-phi))/(aas+bbs)
c            end do
c            call plotit(xq,yq,dum,180,'\\ita\\Rectilinearity test',
c     x     'delay time','fractional variance',2,0,0,0,21)
c            xq(1)=0.
c            xq(2)=90.
c            yq(1)=varfloor
c            yq(2)=varfloor
c            call plotit(xq,yq,dum,2,' ',' ',' ',2,0,0.1,0,0)
c            call plotit(xvar,var,dum,91,'\\ita\\Rectilinearity test',
c     x  'fast-axis strike','fractional variance',2,0,0,0,22)
c          endif
c end trapdoor
c
c          print *,varmax,delt,cper,pi2,are,aim,dt,nip,pc
c          pause
c  fast is expressed as a CCW rotation from the radial component
c  it is more useful to have fast expressed in geographic coordinates
c  strike (CW from N) is conventional, but does not follow GMTplot convention
c  for plotting ellipses: CCW from horizontal
c  
c  we adopt convention that GMT plot has ellipse in geographic coordinates
c
c  SSOOOO... 0 strike --> 90 rotation, -40 strike --> 130 rotation, etc
c            0 baz --> radial is south --> fast=0 --> newfast=-90 or +90
c  --> newfast=fast+90-baz
c
          fast=fast+90.-baz
c  so we are done!  save fast-axis orientation, delt, varmax
          z9(j,nb,4)=fast
          z9(j,nb,5)=delt
          z9(j,nb,6)=ddt
          z9(j,nb,7)=ddth
          z9(j,nb,1)=varmax
        end do
      end do
      return
      end
      subroutine splitest_rect(jpts,nbank,nwh,dt,pc,ip,z9,baz)
c  code to estimate SV/SH splitting using rectilinearity & minimal T comp
c
c  with this in hand
      real*8 pc
      complex*8 za,zu,zv,zz,z0
      common/mwtransf/wavr(24000,3,20,3),wavi(24000,3,20,3)
      common/epars/jink,eang(150,20,2),emaj(150,20,2),emin(150,20,2)
      common/angles/cspsi(200),snpsi(200),coh(100),xvar(100)
      common/ordplot/xq(500),yq(500)
      dimension za(3,3),zu(3,3),zv(3,3),w(3)
      dimension z9(jpts,nbank,7),ip(1)
      pi=3.14159265358979
      pi2=2.*pi
      rdi=360./pi2
      if(nwh.ne.3) then
        print *,'you have ',nwh,' wavelets, not 3.  Go on??'
        pause
      endif
      z0=cmplx(0.,0.)
c  OK, how many ellipses?  5.5 x 2.3 inches
c  2.3 inch / 16 is roughly 0.15 inch --> 5.5/0.15 = 37
c  try 0.12in instead -- aim for 50 ellipses across
c      jink=jpts/55+1
c  for now, dont skip
      jink=6
c  create the sines and cosines
      do i=1,94
        ang=(i-1)
        cspsi(i)=cos(ang/rdi)
        snpsi(i)=sin(ang/rdi)
        xvar(i)=ang
      end do
      do nb=1,nbank
        nip=ip(nb)
        cper=dt*nip/pc
        ji=0
        do j=1,jpts
          do nw=1,nwh
            do m=1,3
              za(m,nw)=cmplx(wavr(j,nw,nb,m),wavi(j,nw,nb,m))
            end do
          end do
c  loop over psi coarsely
c  1=vertical  3=radial, 2=transverse
c  1=fast   2=slow
          cohmax=0.
          ivar=0
          do i=1,91
c          do i=1,91,3
c  rotate components
            do nw=1,nwh
              zu(1,nw)=cspsi(i)*za(3,nw)+snpsi(i)*za(2,nw)
              zu(2,nw)=-snpsi(i)*za(3,nw)+cspsi(i)*za(2,nw)
            end do
c  compute 2x2 cross correlation matrix
            do jj=1,2
              do k=jj,2
                zv(jj,k)=z0
                do nw=1,nwh
                  zv(jj,k)=zv(jj,k)+conjg(zu(jj,nw))*zu(k,nw)  
                end do
              end do
            end do
c  find max coherence of potential fast and slow components
            aa=cabs(zv(1,1))
            bb=cabs(zv(2,2))
            cc=cabs(zv(1,2))
            coh(i)=cc/sqrt(aa*bb)
            if(coh(i).gt.cohmax) then
              cohmax=coh(i)
              aas=aa
              bbs=bb
              ccs=cc
              ivar=i
              zz=zv(1,2)
            endif
          end do 
c  shift ivar back if over 90 degrees
          if(ivar.gt.90) ivar=ivar-90
c  compute the time shift
c  by convention, we would like to keep DT positive, less than one half-cycle
c
          are=real(-zz)
          aim=imag(-zz)
          rad=atan2(aim,are)
          delt=rad*cper/pi2
          fast=ivar-1
c  uncertainty estimate
c  we use the fractional residual var (1.-cohmax^2)/2. to estimate the noise level
c  For time delay, the bestfit is the peak of a sinusoid (at DT0)
c  var(i)=aa*cspsi**2+bb*snpsi**2+2.*cc*snpsi*cspsi*COS(2pi*(DT-DT0)/T)
c  if cc is large, the uncertainty in DT is small
c  if cc is small, the uncertainty in DT is large
c  can obtain uncertainty in degrees via an inv cos
          varno=(1.0-cohmax*cohmax)/2.  
          cohsq=cohmax*cohmax
          if(cohsq.gt.0) then
            cser=(cohsq-varno)/cohsq
          else
            cser=-2.
          endif
          if(cser.le.-1.0) then
            ddt=cper/2.
          else
            ddt=cper*acos(cser)/pi2
          endif
c  for uncertainty in angle of fast axis, we compare values of coh(i) explicitly
          varfloor=cohsq-varno
          do i=ivar,94
            if(coh(i)**2.lt.varfloor) go to 123
          end do
  123     ddth1=i-ivar
          do i=ivar,1,-1
            if(coh(i)**2.lt.varfloor) go to 234
          end do
  234     ddth2=ivar-i
          ddth=amax1(ddth1,ddth2)
          if(ddth1+ddth2.ge.90.) ddth=90.
c  if delt < 0, then we have found the slow axis, not the fast axis
c  we rotate psi CW by 90 degrees, and flip the sign of delt
          if(delt.lt.0.) then
            delt=-delt
            fast=fast-90.
          endif
c  trapdoor to print out coh(i)
c          if(j.eq.jpts/2.and.cper.gt.3.5.and.cper.lt.4.5) then
c            print *,nip,cper,j,fast,delt,cohmax
c            print *,zz,rad
c            do nw=1,nwh
c              do m=1,3
c                za(m,nw)=cmplx(wavr(j,nw,nb,m),wavi(j,nw,nb,m))
c              end do
c            end do
c            print *,za(2,1),za(3,1)
c            print *,za(2,2),za(3,2)
c            print *,za(2,3),za(3,3)
c            xq(1)=0.
c            xq(2)=cper/2.
c            yq(1)=varfloor
c            yq(2)=varfloor
c            call plotit(xq,yq,dum,2,' ',' ',' ',2,0,0.1,0,0)
c            do i=1,360
c              xq(i)=float(i)*cper/360.
c              phi=pi2*delt/cper
c              yq(i)=(aas*cspsi(ivar)**2+bbs*snpsi(ivar)**2+
c     x   2.*cc*snpsi(ivar)*cspsi(ivar)*cos(float(i)/rdi-phi))/(aas+bbs)
c            end do
c            call plotit(xq,yq,dum,180,'\\ita\\Rectilinearity test',
c     x     'delay time','fractional variance',2,0,0,0,21)
c            xq(1)=0.
c            xq(2)=90.
c            yq(1)=varfloor
c            yq(2)=varfloor
c            call plotit(xq,yq,dum,2,' ',' ',' ',2,0,0.1,0,0)
c            call plotit(xvar,var,dum,91,'\\ita\\Rectilinearity test',
c     x  'fast-axis strike','fractional variance',2,0,0,0,22)
c          endif
c end trapdoor
c
c          print *,cohmax,delt,cper,pi2,are,aim,dt,nip,pc
c          pause
c  fast is expressed as a CCW rotation from the radial component
c  it is more useful to have fast expressed in geographic coordinates
c  strike (CW from N) is conventional, but does not follow GMTplot convention
c  for plotting ellipses: CCW from horizontal
c  
c  we adopt convention that GMT plot has ellipse in geographic coordinates
c
c  SSOOOO... 0 strike --> 90 rotation, -40 strike --> 130 rotation, etc
c            0 baz --> radial is south --> fast=0 --> newfast=-90 or +90
c  --> newfast=fast+90-baz
c
          fast=fast+90.-baz
c  so we are done!  save fast-axis orientation, delt, cohmax
          z9(j,nb,4)=fast
          z9(j,nb,5)=delt
          z9(j,nb,6)=ddt
          z9(j,nb,7)=ddth
          z9(j,nb,1)=cohmax
        end do
      end do
      return
      end
      subroutine polar(jpts,nbank,nwh,z9)
      real*8 w,sum,a,b,c,bbcc,amc,apc,bb,disc
      complex*16 za,zu,zv,zz
      common/mwtransf/wavr(24000,3,20,3),wavi(24000,3,20,3)
      common/epars/jink,eang(150,20,2),emaj(150,20,2),emin(150,20,2)
      dimension za(3,3),zu(3,3),zv(3,3),w(3)
      dimension z9(jpts,nbank,7)
      radian=180./3.14159265358979
      if(nwh.ne.3) then
        print *,'you have ',nwh,' wavelets, not 3.  Go on??'
        pause
      endif
c  OK, how many ellipses?  5.5 x 2.3 inches
c  2.3 inch / 16 is roughly 0.15 inch --> 5.5/0.15 = 37
c  try 0.12in instead -- aim for 50 ellipses across
      jink=jpts/55+1
      do nb=1,nbank
        ji=0
        do j=1,jpts
          do nw=1,nwh
            do m=1,3
              za(m,nw)=
     x     dcmplx(dble(wavr(j,nw,nb,m)),dble(wavi(j,nw,nb,m)))
            end do
          end do
c  perform the svd
c        print *,'  perform svd'
c      SUBROUTINE CSVD (A,MMAX,NMAX,M,N,IP,NU,NV,S,U,V)
          call csvd(za,3,nwh,3,nwh,0,nwh,nwh,w,zu,zv)
c  normalize singular values
          sum=0.d0
          do nw=1,nwh
            sum=sum+w(nw)**2
            z9(j,nb,nw)=w(nw)
          end do
          z9(j,nb,4)=w(1)**2/sum
c  save particle motion if appropriate - first eigenvector in zv
c  first 3 components of zu - 1:V 2:T 3:R
          if(j.eq.(j/jink)*jink) then
c  kluge for testing
c           if(z9(j,nb,4).gt.0.95) then
c              print *,j,nb,z9(j,nb,1)
c              print *,'zv'
c              print 109,((zv(i,l),i=1,3),l=1,3)
c              print *,'zu'
c              print 109,((zu(i,l),i=1,3),l=1,3)
c  109 format(3(2f8.3,2x))
c              pause
c            endif
c  end kluge              
            ji=ji+1
            do k=1,2
c testing?  does abs = zabs in the PGI compiler?	  
              c=abs(zu(3,1))
c              c=zabs(zu(3,1))
c  kluge for testing
c              b=zabs(zu(k,1))
c              ang=atan2d(b,c)
c              eang(ji,nb,k)=ang
c              emaj(ji,nb,k)=0.12
c              emin(ji,nb,k)=0.01
c  end kluge              
              zz=c*zu(k,1)/zu(3,1)
              a=dreal(zz)
              b=dimag(zz)
              if(dabs(b).lt.0.003d0*a) then
                eang(ji,nb,k)=radian*datan2(a,c)
                emaj(ji,nb,k)=0.12*dsqrt(a*a+c*c)
                emin(ji,nb,k)=0.01*emaj(ji,nb,k)
              else
                bbcc=b*b*c*c
                apc=(a*a+b*b+c*c)/bbcc
                amc=(a*a+b*b-c*c)/bbcc
                bb=-2*a*c/bbcc
cc  minus sign is reversing rotation of coordinates c.f. Leithold Chap 12.6
cc  still to check: how do I know whether the angle is for the *major* axis?
                ang=radian*datan2(-bb,-amc)
                eang(ji,nb,k)=ang/2.
                disc=dsqrt(amc*amc+bb*bb)
                emaj(ji,nb,k)=0.12*dsqrt(2./(apc-disc))
                aaa=0.12*dsqrt(2./(apc+disc))
                emin(ji,nb,k)=amax1(aaa,0.0011)
              endif
            end do
          endif
c  directly solve for shear-wave splitting parameters
c  the angle gamma from fast axis
c  and phase-delay angle alpha
c  the formulas for these parameters in narrow-band splitting are nonunique
c  but a direct formula for *a* solution can be applied
c  this formula is based on the ratios of 
c  1) the transverse comp, assumed real-valued  T=\sin(2\gamma)\sin\alpha
c  2) the radial comp, assumed complex-valued   
c                          R=-\cos(2\gamma)\sin\alpha - i\cos\alpha
c          c=zabs(zu(2,1))
c          zz=zu(3,1)*(aa/zu(2,1))
c          a=dreal(zz)
c          b=dimag(zz)
c          gamma=0.5*datan(-c/a)
c          alpha=datan(dsqrt(c*c+a*a)/b)
c          if(c.lt.0.d0) alpha=-alpha
        end do
      end do
      return
      end
      subroutine gmtplot0(x9,y9,z9,j9,npts,dt,nx,ny,nami,namlen)
c  writes GMT script to make 4-panel graph
c  bottom:   wavelet spectrum - vertical ("log" colorscale)
c  next up:  wavelet spectrum - transverse ("log" colorscale)
c  next up:  wavelet spectrum - radial ("log" colorscale)
c  top: 3-comp data
      character*(*) nami
      character*84 title,namoz,namon,namoe,namc,namg,namod
      character*200 cmd,cmd2
      common/sing2/a(80000,3)
      common/work/t(80000)
      dimension x9(1),y9(1),z9(1),dmn(3),dmx(3)
c construct output file name.  first look for "/" symbols
c  strip off the '.bh?' of filename
      nlen=namlen-1
      do i=80,1,-1
        if(nami(i:i).eq.'/') then
          islash=i+1
          go to 120
        endif
      end do
      islash=1
  120 nch=nlen-islash+1
      namc='cpt_'//nami(islash:nlen)
      namod='dat_'//nami(islash:nlen)
      namoz='spw_'//nami(islash:nlen)//'Z'
      namon='spw_'//nami(islash:nlen)//'N'
      namoe='spw_'//nami(islash:nlen)//'E'
      namg='GMT_'//nami(islash:nlen)
      title(1:nch)=nami(islash:nlen)
c  find min/max of array
      xmin=x9(1)
      xmax=x9(1)
      ymin=y9(1)
      ymax=y9(1)
      zmin=z9(1)
      zmax=z9(1)
      do i=2,nx
        xmin=amin1(xmin,x9(i))
        xmax=amax1(xmax,x9(i))
      end do
      do i=nx+1,j9,nx
        ymin=amin1(ymin,y9(i))
        ymax=amax1(ymax,y9(i))
      end do
      do i=2,j9
        zmin=amin1(zmin,z9(i))
        zmax=amax1(zmax,z9(i))
      end do
      xmin=0.
c  assumes nskip=6
      dx=6*dt
      xmax=nx*dx
      print *,'xmin,xmax,ymin,ymax,zmin,zmax'
      print *,xmin,xmax,ymin,ymax,zmin,zmax
      dy=(ymax-ymin)/(ny-1)
cc kluge made necessary by GMT "feature" in rev 3.1 and above
c  must not have any slop in the y_inc, ymin, ymax inputs to xyz2grd
c  else the GMT routine EXPLODES!
      idy4=dy*10000
      dy=float(idy4)/10000.
      ymin=ymin-dy/2.
      ymax=ymin+dy*ny
c  scale the data to separate the traceplots
      do k=1,3
        dmn(k)=a(1,k)
        dmx(k)=a(1,k)
        do i=1,npts
          dmn(k)=amin1(dmn(k),a(i,k))
          dmx(k)=amax1(dmx(k),a(i,k))
        end do
      end do
      datmin=dmn(1)
      datshft=0.
      do k=2,3
        datshft=datshft+(dmx(k-1)-dmn(k))*1.1
        do i=1,npts
          a(i,k)=a(i,k)+datshft
        end do
      end do
      datmax=datshft+dmx(3)
      tick=(datmax-datmin)/4.
      datmin=datmin-tick/5.
      datmax=datmax+tick/5.
c  find a sensible tick interval
      ticklog=alog10(tick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        tick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        tick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        tick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        tick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        tick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        tick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        tick=1.5*10.**iticklog
      else
        tick=10.**iticklog
      endif
c  find a sensible xtick interval
      xtick=(xmax-xmin)/6.
      ticklog=alog10(xtick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        xtick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        xtick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        xtick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        xtick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        xtick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        xtick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        xtick=1.5*10.**iticklog
      else
        xtick=10.**iticklog
      endif
c  set places to write text
      xplace=xmin+0.3*xtick
      yplace=ymin+1.5
      if(dt.ge.0.99) yplace=ymax-1.0
c  we do a log scaling for z
c  0 to zmax/scale**3 			white to yellow     
c  zmax/scale**3 to zmax/scale**2 	yellow to red 
c  zmax/scale**2 to zmax/scale	 	red to green
c  zmax/scale to zmax	 		green to blue
      scale=5.
      sc3=scale**(1./3.)
      zz=zmax/scale**4
      zfloor=zz*1.01
      zzz=zz*sc3
      open(8,file=namc,form='formatted')
      write(8,103) zz,' 255 255 255',zzz,' 255 255 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255 255 125',zzz,' 255 255   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255 255   0',zzz,' 125 255 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 255 125',zzz,'   0 255 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,'   0 255 255',zzz,' 125 125 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 125 255',zzz,' 255   0 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0 255',zzz,' 255   0 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0 125',zzz,' 255   0   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0   0',zzz,' 125 125   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 125   0',zzz,'   0 255   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,'   0 255   0',zzz,'   0 125 125'
      zz=zzz
      zzz=zmax*1.002
      write(8,103) zz,'   0 125 125',zzz,'   0   0 255'
  103 format(g11.4,a,1x,g11.4,a)
      close(8)
c  write data to file for plotting
      open(8,file=namod,form='formatted')
      do i=1,npts
        t(i)=i*dt
      end do
      do k=1,3
        write(8,106)(t(i),a(i,k),i=1,npts)
        write(8,102) '>'
      end do
      close(8)
  106 format(f10.2,1x,g11.4)
c  put a floor on the z-value
      do i=1,j9
        z9(i)=amax1(zfloor,z9(i))
      end do
      open(8,file=namoz,form='formatted')
      do i=1,j9/3
        write(8,101) x9(i),y9(i),z9(i)
      end do
      close(8)
      open(8,file=namon,form='formatted')
      do i=j9/3+1,2*(j9/3)
        write(8,101) x9(i),y9(i),z9(i)
      end do
      close(8)
      open(8,file=namoe,form='formatted')
      do i=2*(j9/3)+1,j9
        write(8,101) x9(i),y9(i),z9(i)
      end do
      close(8)
  101 format(g13.6,f11.4,1x,g11.4)
  102 format(80a)
 1022 format(a,f12.2,80a)
      open(8,file=namg,form='formatted')
      write(8,102)'colorfil='//namc
      write(8,102)'infil='//namoz
      write(8,102)'/bin/rm aaplot'
      write(cmd,104)'xyz2grd $infil -Gblobg -I',dx,'/',dy,' -F -R', 
     x  xmin,'/',xmax,'/',ymin,'/',ymax,'  -V'
  104 format(a,f6.2,a,f6.4,a,f6.2,a,f7.2,a,f7.4,a,f7.4,a)
  107 format(2g13.3,a)
      call cleanblanks(cmd)
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil  -B',
     x  xtick,':''time (sec)'':/1.0enSW',
     x  ' -JX5.5/2.3 -X1.0 -Y1.0 -K -P > aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 Vertical'
      write(8,102)'END'
      write(8,102)'infil='//namon
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',
     x  xtick,'/1.0:''period (2@+y@+ sec)'':ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 Transverse'
      write(8,102)'END'
      write(8,102) 'psscale -C$colorfil -D5.8/1.15/3/0.25',
     x   ' -L -O -K -P >> aaplot'
      write(8,102)'infil='//namoe
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',xtick,'/1.0ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 Radial'
      write(8,102)'END'
      write(cmd2,105)'psxy ',namod(1:nch+4),' -R/',xmin,'/',xmax,'/',
     x datmin,'/',datmax,' -B',xtick,':''.',namg(5:nch+1),''':/',
     x  tick,'/ensW -M -JX5.5/2.0 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -O -P <<END>>aaplot'
      yplace=tick/8.
      write(8,107) xplace,yplace,' 10 0 15 1 Vertical'
      yplace=yplace+(dmx(1)-dmn(2))*1.1
      write(8,107) xplace,yplace,' 10 0 15 1 Transverse'
      yplace=yplace+(dmx(2)-dmn(3))*1.1
      write(8,107) xplace,yplace,' 10 0 15 1 Radial'
      write(8,102)'END'
      close(8)
  105 format(3a,f7.3,a,f13.3,a,g13.3,a,g13.3,a,f12.2,3a,g13.3,2a)
      return
      end
      subroutine gmtplot1(x9,y9,z9,j9,npts,dt,nx,ny,nami,namlen)
c  writes GMT script to make 4-panel graph
c  bottom:   splitting parameters - horizontals (varred colorscale)
c  next up:  wavelet spectrum - transverse ("log" colorscale)
c  next up:  wavelet spectrum - radial ("log" colorscale)
c  top: 3-comp data
      character*(*) nami
      character*84 title,namos,namon,namoe,namc,namg,namod,namov
      character*200 cmd,cmd2
      common/sing2/a(80000,3)
      common/work/t(80000)
      common/epars/jink,eang(150,20,2),emj(150,20,2),emin(150,20,2)
      dimension x9(1),y9(1),z9(1),dmn(3),dmx(3),sumd(3)
c construct output file name.  first look for "/" symbols
c  strip off the '.bh?' of filename
      nlen=namlen-1
      do i=80,1,-1
        if(nami(i:i).eq.'/') then
          islash=i+1
          go to 120
        endif
      end do
      islash=1
  120 nch=nlen-islash+1
c  the data & R/T spectra have been written previously by routine gmtplot
c  but we leave this all in while developing the code
      namc='cpt_'//nami(islash:nlen)
      namod='dat_'//nami(islash:nlen)
      namos='spw_'//nami(islash:nlen)//'s'
      namov='spw_'//nami(islash:nlen)//'v'
      namon='spw_'//nami(islash:nlen)//'n'
      namoe='spw_'//nami(islash:nlen)//'e'
      namg='GM1_'//nami(islash:nlen)
      title(1:nch)=nami(islash:nlen)
c  find min/max of array
      xmin=x9(1)
      xmax=x9(1)
      ymin=y9(1)
      ymax=y9(1)
      zmin=z9(j9/3+1)
      zmax=z9(j9/3+1)
      do i=1,nx
        xmin=amin1(xmin,x9(i))
        xmax=amax1(xmax,x9(i))
      end do
      do i=nx+1,j9,nx
        ymin=amin1(ymin,y9(i))
        ymax=amax1(ymax,y9(i))
      end do
      do i=j9/3+2,j9
        zmin=amin1(zmin,z9(i))
        zmax=amax1(zmax,z9(i))
      end do
      xmin=0.
c  assumes nskip=6
      dx=6*dt
      xmax=nx*dx
      print *,'xmin,xmax,ymin,ymax,zmin,zmax'
      print *,xmin,xmax,ymin,ymax,zmin,zmax
      dy=(ymax-ymin)/(ny-1)
cc kluge made necessary by GMT "feature" in rev 3.1 and above
c  must not have any slop in the y_inc, ymin, ymax inputs to xyz2grd
      idy4=dy*10000
      dy=float(idy4)/10000.
      ymin=ymin-dy/2.
      ymax=ymin+dy*ny
c  scale the data to separate the traceplots
      do k=1,3
        sumd(k)=0.
        dmn(k)=a(1,k)
        dmx(k)=a(1,k)
        do i=1,npts
          dmn(k)=amin1(dmn(k),a(i,k))
          dmx(k)=amax1(dmx(k),a(i,k))
          sumd(k)=sumd(k)+a(i,k)
        end do
        sumd(k)=sumd(k)/npts
      end do
      datmin=dmn(1)
      datshft=0.
      do k=2,3
        datshft=datshft+(dmx(k-1)-dmn(k))*1.1
        do i=1,npts
          a(i,k)=a(i,k)+datshft
        end do
      end do
      datmax=datshft+dmx(3)
      tick=(datmax-datmin)/4.
      datmin=datmin-tick/5.
      datmax=datmax+tick/5.
c  find a sensible tick interval
      ticklog=alog10(tick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        tick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        tick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        tick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        tick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        tick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        tick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        tick=1.5*10.**iticklog
      else
        tick=10.**iticklog
      endif
c  find a sensible xtick interval
      xtick=(xmax-xmin)/6.
      ticklog=alog10(xtick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        xtick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        xtick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        xtick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        xtick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        xtick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        xtick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        xtick=1.5*10.**iticklog
      else
        xtick=10.**iticklog
      endif
c  set places to write text
      xplace=xmin+0.3*xtick
      yplace=ymin+1.5
      if(dt.ge.0.99) yplace=ymax-1.0
  103 format(g11.4,a,1x,g11.4,a)
c  the splitting axes are written on color background
c  deep blue for poor rectilinearity, shading to pure white for perfect 
c  since this scale is from 0 to one, dont need to generate file here 
c  it can be found in splitting.cpt
c
c  write out the fast-axes and splitting times as degenerate ellipses
      ezero=0.001
      thresh=0.85
      thresh_ddt=1.25
      thresh_ddth=45.
      open(8,file=namos,form='formatted')
      j5=4*(j9/3)
      j6=5*(j9/3)
      j7=6*(j9/3)
      do nb=1,ny
        yval=y9(nb*nx)
        yval2=2**y9(nb*nx)
        do j=jink,nx,jink
          jj=(nb-1)*nx+j
c  threshhold to whiteout the edge-effect bias
          if(x9(j)-xmin.gt.yval2.and.xmax-x9(j).gt.yval2) then
c  the threshhold skips splitting times that have poor rectilinearity,
c  thresh_ddt*splittime > d(splittime) or d(splitangle) > thresh_ddth
            if(z9(jj).gt.thresh) then
c            if(z9(jj).gt.thresh.and.thresh_ddt*z9(jj+j5).gt.
c     x              z9(jj+j6).and.z9(jj+j7).lt.thresh_ddth) then
c  this choice of normalization leads to 1-sec splitting = 1/8 inch
              emaj=z9(jj+j5)/8.0
              demaj=z9(jj+j6)/8.0
              ddth=z9(jj+j7)
              k=j/jink
      write(8,108) x9(j),yval,z9(j9+jj),emaj,ezero,demaj,ddth
            endif
          endif
        end do
      end do
      close(8)
  108 format(g15.8,g11.3,f8.1,2f6.3,2f8.3)
c  write data to file for plotting
      open(8,file=namod,form='formatted')
      do i=1,npts
        t(i)=i*dt
      end do
      do k=1,3
        write(8,106)(t(i),a(i,k),i=1,npts)
        write(8,102) '>'
      end do
      close(8)
  106 format(f10.2,1x,g11.4)
c  put a floor on the z-value FOR THE SPECTRA - done in gmtplot0
c      zzz=1.001*zmax/scale**4
c      do i=j9/3+1,j9
c        z9(i)=amax1(zzz,z9(i))
c      end do
      open(8,file=namov,form='formatted')
      zblank=1.0
      do i=1,j9/3
c  threshhold to whiteout the edge-effect bias
        if(x9(i)-xmin.gt.2**y9(i).and.xmax-x9(i).gt.2**y9(i)) then
          write(8,101) x9(i),y9(i),z9(i)
        else
          write(8,101) x9(i),y9(i),zblank
        endif
      end do
      close(8)
c  already done in gmtplot0
c      open(8,file=namon,form='formatted')
c      do i=j9/3+1,2*(j9/3)
c        write(8,101) x9(i),y9(i),z9(i)
c      end do
c      close(8)
c      open(8,file=namoe,form='formatted')
c      do i=2*(j9/3)+1,j9
c        write(8,101) x9(i),y9(i),z9(i)
c      end do
c      close(8)
  101 format(g15.8,f11.4,1x,g11.4)
  102 format(80a)
 1022 format(a,f12.2,80a)
  104 format(a,f6.2,a,f6.4,a,f6.2,a,f7.2,a,f7.4,a,f7.4,a)
  107 format(2g13.3,a)
      open(8,file=namg,form='formatted')
C  PANEL #1
      write(8,102)'colorfil=splitting.cpt'
      write(8,102)'infil='//namov
      write(8,102)'/bin/rm aaplot'
      write(cmd,104)'xyz2grd $infil -Gblobg -I',dx,'/',dy,' -F -R', 
     x  xmin,'/',xmax,'/',ymin,'/',ymax,'  -V'
      call cleanblanks(cmd)
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil  -B',
     x  xtick,':''time (sec)'':/1.0enSW',
     x  ' -JX5.5/2.3 -X1.0 -Y1.0 -K -P > aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'psxy ',namos,
     x      ' -W5 -L -Se1 -W8 -JX -R -O -P -K >>aaplot'
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 Splitting'
      write(8,102)'END'
c  LEGEND FOR SPLITTING DIAGRAM
      write(8,102) 'psxy -Jx1.0/1.0 -X0.0 -Y0.0 -Se1 -W8',
     x  ' -R0.0/7.0/0.0/2.3 -V -O -K <<END>> aaplot'
c  scale is 1-sec to 1/8 inch
      write(8,102) '6.0 1.0 45 0.125 0.001'
      write(8,102) 'END'
      write(8,102) 'pstext -Jx -R -V -O -K <<END>> aaplot'
      write(8,102) '6.0 0.8 14 0 15 1 1-sec'
      write(8,102) '6.0 0.6 14 0 15 1 splitting'
      write(8,102) '6.0 0.4 14 0 15 1 45\\312 strike'
      write(8,102) 'END'
C  PANEL #2
      write(8,102)'colorfil='//namc
      write(8,102)'infil='//namon
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',
     x  xtick,'/1.0:''period (2@+y@+ sec)'':ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 Transverse'
      write(8,102)'END'
C  COLORSCALE
      write(8,102) 'psscale -C$colorfil -D5.8/1.15/3/0.25',
     x   ' -L -O -K -P >> aaplot'
C  PANEL #3
      write(8,102)'infil='//namoe
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',xtick,'/1.0ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 Radial'
      write(8,102)'END'
C  PANEL #4
      write(cmd2,105)'psxy ',namod(1:nch+4),' -R/',xmin,'/',xmax,'/',
     x datmin,'/',datmax,' -B',xtick,':''.',namg(5:nch+1),''':/',
     x  tick,'/ensW -M -JX5.5/2.0 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -O -P <<END>>aaplot'
      yplace=tick/8.
      write(8,107) xplace,yplace,' 10 0 15 1 Vertical'
      yplace=yplace+(dmx(1)-dmn(2))*1.1+sumd(2)
      write(8,107) xplace,yplace,' 10 0 15 1 Transverse'
      yplace=yplace+(dmx(2)-dmn(3))*1.1+sumd(3)-sumd(2)
      write(8,107) xplace,yplace,' 10 0 15 1 Radial'
      write(8,102)'END'
      close(8)
  105 format(3a,f6.2,a,f13.3,a,g13.3,a,g13.3,a,f12.2,3a,g13.3,2a)
      return
      end
      subroutine gmtplot2(x9,y9,z9,j9,npts,dt,nx,ny,nami,namlen)
c  writes GMT script to make 4-panel graph
c  bottom:  3rd singular value ("log" colorscale)
c  next up: 2nd singular value ("log" colorscale)
c  next up: 1st singular value ("log" colorscale)
c  top: 3-comp data
      character*(*) nami
      character*84 title,namo1,namo2,namo3,namc,namg,namod
      character*200 cmd,cmd2
      common/sing2/a(80000,3)
      common/work/t(80000)
      dimension x9(1),y9(1),z9(1),dmn(3),dmx(3),sumd(3)
c construct output file name.  first look for "/" symbols
c  strip off the '.bh?' of filename
      nlen=namlen-1
      do i=80,1,-1
        if(nami(i:i).eq.'/') then
          islash=i+1
          go to 120
        endif
      end do
      islash=1
  120 nch=nlen-islash+1
      namc='cp2_'//nami(islash:nlen)
      namod='dat_'//nami(islash:nlen)
      namo1='lam_'//nami(islash:nlen)//'1'
      namo2='lam_'//nami(islash:nlen)//'2'
      namo3='lam_'//nami(islash:nlen)//'3'
      namg='GM2_'//nami(islash:nlen)
      title(1:nch)=nami(islash:nlen)
c  find min/max of array
      xmin=x9(1)
      xmax=x9(1)
      ymin=y9(1)
      ymax=y9(1)
      zmin=z9(1)
      zmax=z9(1)
      do i=2,nx
        xmin=amin1(xmin,x9(i))
        xmax=amax1(xmax,x9(i))
      end do
      do i=nx+1,j9,nx
        ymin=amin1(ymin,y9(i))
        ymax=amax1(ymax,y9(i))
      end do
      do i=2,j9
        zmin=amin1(zmin,z9(i))
        zmax=amax1(zmax,z9(i))
      end do
      xmin=0.
c  assumes nskip=6
      dx=6*dt
      xmax=nx*dx
      print *,'xmin,xmax,ymin,ymax,zmin,zmax'
      print *,xmin,xmax,ymin,ymax,zmin,zmax
      dy=(ymax-ymin)/(ny-1)
cc kluge made necessary by GMT "feature" in rev 3.1 and above
c  must not have any slop in the y_inc, ymin, ymax inputs to xyz2grd
      idy4=dy*10000
      dy=float(idy4)/10000.
      ymin=ymin-dy/2.
      ymax=ymin+dy*ny
c  scale the data to separate the traceplots
      do k=1,3
        sumd(k)=0.
        dmn(k)=a(1,k)
        dmx(k)=a(1,k)
        do i=1,npts
          dmn(k)=amin1(dmn(k),a(i,k))
          dmx(k)=amax1(dmx(k),a(i,k))
          sumd(k)=sumd(k)+a(i,k)
        end do
        sumd(k)=sumd(k)/npts
      end do
      datmin=dmn(1)
      datshft=0.
      do k=2,3
        datshft=datshft+(dmx(k-1)-dmn(k))*1.1
        do i=1,npts
          a(i,k)=a(i,k)+datshft
        end do
      end do
      datmax=datshft+dmx(3)
      tick=(datmax-datmin)/4.
      datmin=datmin-tick/5.
      datmax=datmax+tick/5.
c  find a sensible tick interval
      ticklog=alog10(tick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        tick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        tick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        tick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        tick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        tick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        tick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        tick=1.5*10.**iticklog
      else
        tick=10.**iticklog
      endif
c  find a sensible xtick interval
      xtick=(xmax-xmin)/6.
      ticklog=alog10(xtick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        xtick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        xtick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        xtick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        xtick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        xtick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        xtick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        xtick=1.5*10.**iticklog
      else
        xtick=10.**iticklog
      endif
c  set places to write text
      xplace=xmin+0.3*xtick
      yplace=ymin+1.5
      if(dt.ge.0.99) yplace=ymax-1.0
c  we do a log scaling for z
c  0 to zmax/scale**3 			white to yellow     
c  zmax/scale**3 to zmax/scale**2 	yellow to red 
c  zmax/scale**2 to zmax/scale	 	red to green
c  zmax/scale to zmax	 		green to blue
      scale=5.
      sc3=scale**(1./3.)
      zz=zmax/scale**4
      zzz=zz*sc3
      open(8,file=namc,form='formatted')
      write(8,103) zz,' 255 255 255',zzz,' 255 255 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255 255 125',zzz,' 255 255   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255 255   0',zzz,' 125 255 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 255 125',zzz,'   0 255 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,'   0 255 255',zzz,' 125 125 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 125 255',zzz,' 255   0 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0 255',zzz,' 255   0 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0 125',zzz,' 255   0   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0   0',zzz,' 125 125   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 125   0',zzz,'   0 255   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,'   0 255   0',zzz,'   0 125 125'
      zz=zzz
      zzz=zmax*1.002
      write(8,103) zz,'   0 125 125',zzz,'   0   0 255'
  103 format(g11.4,a,1x,g11.4,a)
      close(8)
c  write data to file for plotting
      open(8,file=namod,form='formatted')
      do i=1,npts
        t(i)=i*dt
      end do
      do k=1,3
        write(8,106)(t(i),a(i,k),i=1,npts)
        write(8,102) '>'
      end do
      close(8)
  106 format(f10.2,1x,g11.4)
c  put a floor on the z-value
      zzz=1.001*zmax/scale**4
      do i=1,j9
        z9(i)=amax1(zzz,z9(i))
      end do
      open(8,file=namo1,form='formatted')
      do i=1,j9/3
        write(8,101) x9(i),y9(i),z9(i)
      end do
      close(8)
      open(8,file=namo2,form='formatted')
      do i=j9/3+1,2*(j9/3)
        write(8,101) x9(i),y9(i),z9(i)
      end do
      close(8)
      open(8,file=namo3,form='formatted')
      do i=2*(j9/3)+1,j9
        write(8,101) x9(i),y9(i),z9(i)
      end do
      close(8)
  101 format(g15.8,f11.4,1x,g11.4)
  102 format(80a)
 1022 format(a,f12.2,80a)
      open(8,file=namg,form='formatted')
      write(8,102)'colorfil='//namc
      write(8,102)'infil='//namo3
      write(8,102)'/bin/rm aaplot'
      write(cmd,104)'xyz2grd $infil -Gblobg -I',dx,'/',dy,' -F -R', 
     x  xmin,'/',xmax,'/',ymin,'/',ymax,'  -V'
  104 format(a,f6.2,a,f6.4,a,f6.2,a,f7.2,a,f7.4,a,f7.4,a)
  107 format(2g13.3,a)
      call cleanblanks(cmd)
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil  -B',
     x  xtick,':''time (sec)'':/1.0enSW',
     x  ' -JX5.5/2.3 -X1.0 -Y1.0 -K -P > aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 30 0 15 1 @~l@~@-3@-'
      write(8,102)'END'
      write(8,102)'infil='//namo2
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',
     x  xtick,'/1.0:''period (2@+y@+ sec)'':ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 30 0 15 1 @~l@~@-2@-'
      write(8,102)'END'
      write(8,102) 'psscale -C$colorfil -D5.8/1.15/3/0.25',
     x   ' -L -O -K -P >> aaplot'
      write(8,102)'infil='//namo1
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',xtick,'/1.0ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 30 0 15 1 @~l@~@-1@-'
      write(8,102)'END'
      write(cmd2,105)'psxy ',namod(1:nch+4),' -R/',xmin,'/',xmax,'/',
     x datmin,'/',datmax,' -B',xtick,':''.',namg(5:nch+1),''':/',
     x  tick,'/ensW -M -JX5.5/2.0 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -O -P <<END>>aaplot'
      yplace=tick/8.
      write(8,107) xplace,yplace,' 10 0 15 1 Vertical'
      yplace=yplace+(dmx(1)-dmn(2))*1.1+sumd(2)
      write(8,107) xplace,yplace,' 10 0 15 1 Transverse'
      yplace=yplace+(dmx(2)-dmn(3))*1.1+sumd(3)-sumd(2)
      write(8,107) xplace,yplace,' 10 0 15 1 Radial'
      write(8,102)'END'
      close(8)
  105 format(3a,f6.2,a,f13.3,a,g13.3,a,g13.3,a,f13.3,3a,g13.3,2a)
      return
      end
      subroutine gmtplot3(x9,y9,z9,j9,npts,dt,nx,ny,nami,namlen)
c  writes GMT script to make 4-panel graph
c  bottom: normalized 1st singular value (bluescale)
c  next up: normalized 1st singular value (bluescale)
c  next up: 1st singular value ("log" colorscale)
c  top: 3-comp data
      character*(*) nami
      character*84 title,namo1,namo0,namc,namg,namod,nampz,nampt
      character*200 cmd,cmd2
      common/sing2/a(80000,3)
      common/epars/jink,eang(150,20,2),emaj(150,20,2),emin(150,20,2)
      common/work/t(80000)
      dimension x9(1),y9(1),z9(1),dmn(3),dmx(3),sumd(3)
      thresh=0.7
c construct output file name.  first look for "/" symbols
c  strip off the '.bh?' of filename
      nlen=namlen-1
      do i=80,1,-1
        if(nami(i:i).eq.'/') then
          islash=i+1
          go to 120
        endif
      end do
      islash=1
  120 nch=nlen-islash+1
      namc='cp2_'//nami(islash:nlen)
      namod='dat_'//nami(islash:nlen)
      namo1='lam_'//nami(islash:nlen)//'1'
      namo0='lam_'//nami(islash:nlen)//'0'
      nampz='ell_'//nami(islash:nlen)//'Z'
      nampt='ell_'//nami(islash:nlen)//'T'
      namg='GM3_'//nami(islash:nlen)
      title(1:nch)=nami(islash:nlen)
c  find min/max of array
      xmin=x9(1)
      xmax=x9(1)
      ymin=y9(1)
      ymax=y9(1)
      zmin=z9(1)
      zmax=z9(1)
      do i=2,nx
        xmin=amin1(xmin,x9(i))
        xmax=amax1(xmax,x9(i))
      end do
      do i=nx+1,j9,nx
        ymin=amin1(ymin,y9(i))
        ymax=amax1(ymax,y9(i))
      end do
      do i=2,j9
        zmin=amin1(zmin,z9(i))
        zmax=amax1(zmax,z9(i))
      end do
      xmin=0.
c  assumes nskip=6
      dx=6*dt
      xmax=nx*dx
      print *,'xmin,xmax,ymin,ymax,zmin,zmax'
      print *,xmin,xmax,ymin,ymax,zmin,zmax
      dy=(ymax-ymin)/(ny-1)
cc kluge made necessary by GMT "feature" in rev 3.1 and above
c  must not have any slop in the y_inc, ymin, ymax inputs to xyz2grd
      idy4=dy*10000
      dy=float(idy4)/10000.
      ymin=ymin-dy/2.
      ymax=ymin+dy*ny
c  scale the data to separate the traceplots
      do k=1,3
        sumd(k)=0.
        dmn(k)=a(1,k)
        dmx(k)=a(1,k)
        do i=1,npts
          dmn(k)=amin1(dmn(k),a(i,k))
          dmx(k)=amax1(dmx(k),a(i,k))
          sumd(k)=sumd(k)+a(i,k)
        end do
        sumd(k)=sumd(k)/npts
      end do
      datmin=dmn(1)
      datshft=0.
      do k=2,3
        datshft=datshft+(dmx(k-1)-dmn(k))*1.1
        do i=1,npts
          a(i,k)=a(i,k)+datshft
        end do
      end do
      datmax=datshft+dmx(3)
      tick=(datmax-datmin)/4.
      datmin=datmin-tick/5.
      datmax=datmax+tick/5.
c  find a sensible tick interval
      ticklog=alog10(tick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        tick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        tick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        tick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        tick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        tick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        tick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        tick=1.5*10.**iticklog
      else
        tick=10.**iticklog
      endif
c  find a sensible xtick interval
      xtick=(xmax-xmin)/6. 
      ticklog=alog10(xtick)
      iticklog=ticklog
      ttick=ticklog-iticklog
      if(ttick.gt.alog10(7.5)) then
        xtick=7.5*10.**iticklog
      elseif(ttick.gt.alog10(5.)) then
        xtick=5.0*10.**iticklog
      elseif(ttick.gt.alog10(4.)) then
        xtick=4.0*10.**iticklog
      elseif(ttick.gt.alog10(3.)) then
        xtick=3.0*10.**iticklog
      elseif(ttick.gt.alog10(2.5)) then
        xtick=2.5*10.**iticklog
      elseif(ttick.gt.alog10(2.0)) then
        xtick=2.0*10.**iticklog
      elseif(ttick.gt.alog10(1.5)) then
        xtick=1.5*10.**iticklog
      else
        xtick=10.**iticklog
      endif
c  set places to write text
      xplace=xmax-1.3*xtick
      yplace=ymin+1.5
c      if(dt.ge.0.99) yplace=ymax-1.0
c  we do a log scaling for z
c  0 to zmax/scale**3 			white to yellow     
c  zmax/scale**3 to zmax/scale**2 	yellow to red 
c  zmax/scale**2 to zmax/scale	 	red to green
c  zmax/scale to zmax	 		green to blue
      scale=5.
      sc3=scale**(1./3.)
      zz=zmax/scale**4
      zzz=zz*sc3
      open(8,file=namc,form='formatted')
      write(8,103) zz,' 255 255 255',zzz,' 255 255 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255 255 125',zzz,' 255 255   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255 255   0',zzz,' 125 255 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 255 125',zzz,'   0 255 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,'   0 255 255',zzz,' 125 125 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 125 255',zzz,' 255   0 255'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0 255',zzz,' 255   0 125'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0 125',zzz,' 255   0   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 255   0   0',zzz,' 125 125   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,' 125 125   0',zzz,'   0 255   0'
      zz=zzz
      zzz=zzz*sc3
      write(8,103) zz,'   0 255   0',zzz,'   0 125 125'
      zz=zzz
      zzz=zmax*1.002
      write(8,103) zz,'   0 125 125',zzz,'   0   0 255'
  103 format(g11.4,a,1x,g11.4,a)
      close(8)
c  write bluestep.cpt - colorfile for normalized first singular value
      open(8,file='bluestep.cpt',form='formatted')
c  deep blue is strong polarization
c      write(8,102) '0.00 255 255 255 0.70 255 255 255'
c      write(8,102) '0.70 225 225 255 0.75 215 225 255'
c      write(8,102) '0.75 200 200 255 0.80 200 200 255'
c      write(8,102) '0.80 175 175 255 0.85 175 175 255'
c      write(8,102) '0.85 150 150 255 0.90 150 150 255'
c      write(8,102) '0.90  90  90 255 0.95  90  90 255'
c      write(8,102) '0.95   0   0 200 1.00   0   0 200'
c  deep white is strong polarization
      write(8,102) '0.00   0   0 200 0.70   0   0 200'
      write(8,102) '0.70  90  90 255 0.75  90  90 255'
      write(8,102) '0.75 150 150 255 0.80 150 150 255'
      write(8,102) '0.80 175 175 255 0.85 175 175 255'
      write(8,102) '0.85 200 200 255 0.90 200 200 255'
      write(8,102) '0.90 225 225 255 0.95 225 225 255'
      write(8,102) '0.95 255 255 255 1.00 255 255 255'
      close(8)
c  write data to file for plotting
      open(8,file=namod,form='formatted')
      do i=1,npts
        t(i)=i*dt
      end do
      do k=1,3
        write(8,106)(t(i),a(i,k),i=1,npts)
        write(8,102) '>'
      end do
      close(8)
  106 format(f10.2,1x,g11.4)
c  put a floor on the z-value (only lambda1)
      zzz=1.001*zmax/scale**4
      do i=1,j9/3
        z9(i)=amax1(zzz,z9(i))
      end do
c  write out the first singular value
      open(8,file=namo1,form='formatted')
      do i=1,j9/3
        write(8,101) x9(i),y9(i),z9(i)
      end do
      close(8)
c  write out the normalized first singular value
      open(8,file=namo0,form='formatted')
      do i=1,j9/3
        write(8,101) x9(i),y9(i),z9(j9+i)
      end do
      close(8)
c  write out the ellipses, but only if they exceed threshhold
      print *,jink
      open(8,file=nampz,form='formatted')
      do nb=1,ny
        do j=jink,nx,jink
          jj=j9+(nb-1)*nx+j
          if(z9(jj).gt.thresh) then
            k=j/jink
            write(8,108) x9(j),y9(nb*nx),
     x                  eang(k,nb,1),emaj(k,nb,1),emin(k,nb,1)
          endif
        end do
      end do
      close(8)
      open(8,file=nampt,form='formatted')
      do nb=1,ny
        do j=jink,nx,jink
          jj=j9+(nb-1)*nx+j
          if(z9(jj).gt.thresh) then
            k=j/jink
            write(8,108) x9(j),y9(nb*nx),
     x                  eang(k,nb,2),emaj(k,nb,2),emin(k,nb,2)
          endif
        end do
      end do
      close(8)
  101 format(g15.8,f11.4,1x,g11.4)
  108 format(g15.8,g11.3,f8.1,2f6.3)
  102 format(80a)
 1022 format(a,f12.2,80a)
      open(8,file=namg,form='formatted')
      write(8,102)'colorfil=bluestep.cpt'
      write(8,102)'infil='//namo0
      write(8,102)'/bin/rm aaplot'
      write(cmd,104)'xyz2grd $infil -Gblobg -I',dx,'/',dy,' -F -R', 
     x  xmin,'/',xmax,'/',ymin,'/',ymax,'  -V'
  104 format(a,f6.2,a,f6.4,a,f6.2,a,f7.2,a,f7.4,a,f7.4,a)
  107 format(2g13.3,a)
      call cleanblanks(cmd)
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil  -B',
     x  xtick,':''time (sec)'':/1.0enSW',
     x  ' -JX5.5/2.3 -X1.0 -Y1.0 -K -P > aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'psxy ',nampt,
     x      ' -W5 -L -Se1 -W4 -JX -R -O -P -K >>aaplot'
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 R/T ellipse'
      write(8,102)'END'
      write(8,102) 'psscale -C$colorfil -D5.8/1.15/3/0.25',
     x   ' -L -O -K -P >> aaplot'
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',
     x  xtick,'/1.0:''period (2@+y@+ sec)'':ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
      call cleanblanks(cmd2)
      write(8,102) cmd2
      write(8,102) 'psxy ',nampz,
     x      ' -W5 -L -Se1 -W4 -JX -R -O -P -K >>aaplot'
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 15 0 15 1 R/Z ellipse'
      write(8,102)'END'
      write(8,102)'colorfil='//namc
      write(8,102)'infil='//namo1
      write(8,102) cmd
      write(cmd2,1022)'grdimage blobg -C$colorfil -B',xtick,'/1.0ensW',
     x  ' -JX5.5/2.3 -X0.0 -Y2.3 -O -K -P >> aaplot'
c      print *,cmd2
      call cleanblanks(cmd2)
c      print *,cmd2
      write(8,102) cmd2
      xplace=xmax-1.3*xtick
      yplace=ymin+1.5
      if(dt.ge.0.99) yplace=ymax-1.0
      write(8,102) 'pstext -JX -R -W -O -P -K <<END>>aaplot'
      write(8,107) xplace,yplace,' 20 0 15 1 @~l@~@-1@-'
      write(8,102)'END'
      write(8,102) 'psscale -C$colorfil -D5.8/1.15/3/0.25',
     x   ' -L -O -K -P >> aaplot'
      write(cmd2,105)'psxy ',namod(1:nch+4),' -R',xmin,'/',xmax,'/',
     x datmin,'/',datmax,' -B',xtick,':''.',namg(5:nch+1),''':/',
     x  tick,'/ensW -M -JX5.5/2.0 -X0.0 -Y2.3 -O -K -P >> aaplot'
c      print *,cmd2
      call cleanblanks(cmd2)
c      print *,cmd2
c      pause
      write(8,102) cmd2
      write(8,102) 'pstext -JX -R -O -P <<END>>aaplot'
      yplace=tick/8.
      write(8,107) xplace,yplace,' 10 0 15 1 Vertical'
      yplace=yplace+(dmx(1)-dmn(2))*1.1+sumd(2)
      write(8,107) xplace,yplace,' 10 0 15 1 Transverse'
      yplace=yplace+(dmx(2)-dmn(3))*1.1+sumd(3)-sumd(2)
      write(8,107) xplace,yplace,' 10 0 15 1 Radial'
      write(8,102)'END'
      close(8)
  105 format(3a,f6.2,a,f13.3,a,g13.3,a,g13.3,a,f12.2,3a,g13.3,2a)
      return
      end
      subroutine cleanblanks(cmd)
      character*200 cmd
c  clean up the blanks
c      print *,cmd
      i=1
      do while(i.le.199)
  110   continue
        if(cmd(i:i+1).eq.'-R'.and.cmd(i+2:i+2).eq.' ') then
          cmd=cmd(1:i+1)//cmd(i+3:200)//' '
c          print *,cmd
          go to 110
        endif
        i=i+1
      end do
      i=1
      do while(i.le.199)
  111   continue
        if(cmd(i:i+1).eq.'-I'.and.cmd(i+2:i+2).eq.' ') then
          cmd=cmd(1:i+1)//cmd(i+3:200)//' '
c          print *,cmd
          go to 111
        endif
        i=i+1
      end do
      i=1
      do while(i.le.199)
  112   continue
        if(cmd(i:i).eq.'/'.and.cmd(i+1:i+1).eq.' ') then
          cmd=cmd(1:i)//cmd(i+2:200)//' '
c          print *,cmd
          go to 112
        endif
        i=i+1
      end do
      i=200
      do while(i.gt.2)
  114   continue
        if(cmd(i:i).eq.'/'.and.cmd(i-1:i-1).eq.' ') then
          cmd=cmd(1:i-2)//cmd(i:200)//' '
c          print *,cmd
          go to 114
        endif
        i=i-1
      end do
      i=1
      do while(i.le.100)
  113   continue
        if(cmd(i:i+1).eq.'-B'.and.cmd(i+2:i+2).eq.' ') then
          cmd=cmd(1:i+1)//cmd(i+3:200)//' '
c          print *,cmd
          go to 113
        endif
        i=i+1
      end do
c      print *,cmd
      return
      end
      subroutine slepwav(p,pc,nwav,n,wav)
c  to transfer Lilly and Park wavelet algorithm to fortran code for 
c  general use - calculates set of wavelets for a specified number of points
c  codes that use wavelets must interpolate and fft other lengths on the fly
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      real*4 wav
c  12 real-wavelets max = 6 complex-valued wavelets
      common/wave1/a(1000000),w(1000),z(12000)
      dimension wav(1)
c  first, create the slepian wavelets, 
      ann=float(n)
      if(n.gt.1000) then
        print *,n,' too big!!!'
        stop
      endif
  103 format(15i5)
      call fillmat(n,p,pc,a)
      call eis(nwav,n,p,pc,a,w,z,wav)
      return
      end
      subroutine fillmat(n,p,pc,a)
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      dimension a(n,1)
c  fill first column of matrix
      pi2=2.d0*3.14159256358979d0
      an=dfloat(n)
      fw=p/an
      fo=pc/an
      do i=1,n-1
        a(i+1,1)=4.d0*dsin(pi2*i*fw)*dcos(pi2*i*fo)/(pi2*i)
      end do
      a(1,1)=4.d0*fw
c  fill out the Toeplitz matrix
      do j=2,n
        do i=1,j-1
          a(i,j)=a(j,i)
        end do
        do i=j,n
          a(i,j)=a(i-j+1,1)
        end do 
      end do
c      do i=1,n
c        print 101,(a(j,i),j=1,n)
c      end do
c  101 format(8g11.4)
      return
      end
      subroutine eis(nwav,n,p,pc,a,w,z,wav)
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      real*4 b4,x4(3),wav
      dimension a(n,1),w(1),z(n,1),wav(n,1)
      common/work2/ab(1000),bc(1000),cd(1000),de(1000),ef(1000)
      common/work3/fg(1000),gh(1000),hi(1000),einv(1000),ia(1000)
      common/work4/b4(2050),br4(2050),bi4(2050)
      sq2=dsqrt(2.d0)
c  make wavelets
      call tred1(n,n,a,ab,bc,cd)
      eps1=1.d-16
      vhigh=1.1d4
      valow=0.9d0
      call bisect(n,eps1,ab,bc,cd,valow,vhigh,n,ngot,w,ia,ier,de,ef)
      if(ier.ne.0) go to 999
      call tinvit(n,n,ab,bc,cd,ngot,w,ia,z,ier,de,ef,fg,gh,hi)
      if(ier.ne.0) go to 999
      call trbak1(n,n,a,bc,ngot,z)
c      print 101,ngot
  101 format('# of eigenwindows ',i4)
c      print*,'eigenvalues'
c      print 102,(w(ngot+1-i),i=1,ngot)
  102 format(7f15.10)
  108 format(10e12.3)
  103 format(20a4)
c      print *,nwav,ngot,n,p,pc
c  we combine the even and odd wavelets to form a complex wavelet
c  divide by sqrt(2) to retain unit-normalization
      do nw=1,nwav,2
        do i=1,n
          wav(i,nw)=z(i,ngot+1-nw)/sq2
        end do
        do i=1,n
          wav(i,nw+1)=z(i,ngot-nw)/sq2
        end do
c  check that b4 is even & bi4 is odd, if not, reverse
        bb4=wav(n/2,nw)*wav(n/2+1,nw)
        bibi4=wav(n/2,nw+1)*wav(n/2+1,nw+1)
        if(bb4.le.0.d0.or.bibi4.ge.0.d0) then
c          print *,'switch even and odd wavelets!',bb4,bibi4
          do i=1,n
            wav(i,nw+1)=z(i,ngot+1-nw)/sq2
          end do
          do i=1,n
            wav(i,nw)=z(i,ngot-nw)/sq2
          end do
        endif
c  next, we make complex wavelet consistent with exp(-i\omega t) convention
c  at real-part max (phi=0), the imag part has negative slope thru zero.
c  note: the wavelet that we store is the version used to reconstruct signals 
c  in the time domain. Its dual vector is the complex conjugate, 
c  which is convolved with the data series 
c  to transform it from the time to the wavelet domain
        if(wav(n/2,nw).lt.0.d0) then
          do i=1,n
            wav(i,nw)=-wav(i,nw)
          end do
        endif
c  try a time shift and dot product 
        sum=0.d0
        do i=1,n-1
          sum=sum+wav(i,nw)*wav(i+1,nw+1)
        end do
        if(sum.gt.0.d0) then
          do i=1,n
            wav(i,nw+1)=-wav(i,nw+1)
          end do
        endif
      end do
c  plot the wavelets
c      x4(1)=1.
c      x4(2)=1.
c      x4(3)=1.
c      do nw=1,nwav
c        do i=1,n
c          b4(i)=wav(i,nw)
c        end do
c        call plotit(x4,b4,dum,n,'wavelets','time',' ',1,0,(nw-1)*.04,
c     x                                                0,21*(nw/nwav))
c      end do
c      do nw=1,nwav,2
c        do i=1,n
c          b4(i)=sqrt(wav(i,nw)**2+wav(i,nw+1)**2)
c        end do
c        call plotit(x4,b4,dum,n,'wavelet envelopes',
c     x   'time',' ',1,0,(nw-1)*.04,0,22*((nw+1)/nwav))
c      end do
      return
  999 print *,ier
      stop
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c               routine
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fft2(ar,ai,n)
c  fft routine with 2 real input arrays rather than complex - see ffttwo
c  for comments. 
c  fft2 subroutine mults by exp(i\omega t)
c  OUTPUT: f=0 in ar(1), f=f_N in ar(n/2+1)
c          ar(i)=ar(n+2-i), ai(i)=-ai(n+2-i)
c  fft2 is NOT a unitary transform, mults the series by sqrt(n)
c  the inverse FT can be effected by running fft2 on the conjugate of
c  the FFT-expansion, then taking the the conjugate of the output, 
c  and dividing thru by N. to wit:
c
c   assume Xr, Xi is in freq domain, xr, xi in the time domain
c
c   (Xr,Xi)=fft2(xr,xi,N)
c   (xr,-xi)=fft2(Xr,-Xi,N)/N
c
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      dimension ar(1),ai(1)
      mex=dlog(dble(float(n)))/.693147d0
      nv2=n/2
      nm1=n-1
      j=1
      do 7 i=1,nm1
      if(i .ge. j) go to 5
      tr=ar(j)
      ti=ai(j)
      ar(j)=ar(i)
      ai(j)=ai(i)
      ar(i)=tr
      ai(i)=ti
   5  k=nv2
   6  if(k .ge. j) go to 7
      j=j-k
      k=k/2
      go to 6
   7  j=j+k
      pi=3.14159265358979d0
      do 20 l=1,mex
      le=2**l
      le1=le/2
      ur=1.0
      ui=0.
      wr=dcos(pi/le1 )
      wi=dsin (pi/le1)
      do 20 j=1,le1
      do 10 i=j,n,le
      ip=i+le1
      tr=ar(ip)*ur - ai(ip)*ui
      ti=ai(ip)*ur + ar(ip)*ui
      ar(ip)=ar(i)-tr
      ai(ip)=ai(i) - ti
      ar(i)=ar(i)+tr
      ai(i)=ai(i)+ti
  10  continue
      utemp=ur
      ur=ur*wr - ui*wi
      ui=ui*wr + utemp*wi
  20  continue
      return
      end
      SUBROUTINE CSVD (A,MMAX,NMAX,M,N,IP,NU,NV,S,U,V)
C
C   Singular value decomposition of an  M by N  complex matrix  A,
C   where M .GT. N .  The singular values are stored in the vector
C   S. The first NU columns of the M by M unitary matrix U and the
C   first NV columns of the N by N unitary matrix V  that minimize
C   Det(A-USV*) are also computed.
C
C
C         P.A. Businger and G.H. Golub, "Singular Value Decomposition
C         of a Complex Matrix," Communications of the ACM, vol. 12,
C         pp. 564-565, October 1969.
C
C   This algorithm is reprinted by permission, Association for
C   Computing Machinery; copyright 1969.
C
      COMPLEX *16 A(MMAX,NMAX),U(MMAX,MMAX),V(NMAX,NMAX),Q,R
      REAL *8 S(NMAX),B(2000),C(2000),T(2000),zero,one,ETA,TOL,
     $        EPS
      ETA = 1.2d-7
      TOL = 2.4d-32
      zero = 0.d0
      one = 1.d0
      NP=N+IP
      N1=N+1
C   Householder reduction
      C(1)=zero
      K=1
10    K1=K+1
C   Elimination of A(I,K) , I=K+1,...,M
      Z=zero
      DO 20 I=K,M
20      Z=Z+dreal(A(I,K))**2+dIMAG(A(I,K))**2
      B(K)=zero
      IF (Z .LE. TOL)  GO TO 70
      Z=SQRT(Z)
      B(K)=Z
      W=CdABS(A(K,K))
      Q=dcmplx(one,zero)
      IF (W .NE. zero)  Q=A(K,K)/W
      A(K,K)=Q*(Z+W)
      IF (K .EQ. NP)  GO TO 70
      DO 50 J=K1,NP
        Q=dcmplx(zero,zero)
        DO 30 I=K,M
30        Q=Q+dCONJG(A(I,K))*A(I,J)
        Q=Q/(Z*(Z+W))
        DO 40 I=K,M
40        A(I,J)=A(I,J)-Q*A(I,K)
50      CONTINUE
C   Phase transformation
      Q=-dCONJG(A(K,K))/CdABS(A(K,K))
      DO 60 J=K1,NP
60      A(K,J)=Q*A(K,J)
C   Elimination of A(K,J) , J=K+2,...,N
70    IF (K .EQ. N)  GO TO 140
      Z=zero
      DO 80 J=K1,N
80      Z=Z+dreal(A(K,J))**2+dIMAG(A(K,J))**2
      C(K1)=zero
      IF (Z .LE. TOL)  GO TO 130
      Z=SQRT(Z)
      C(K1)=Z
      W=CdABS(A(K,K1))
      Q=dcmplx(one,zero)
      IF (W .NE. zero)  Q=A(K,K1)/W
      A(K,K1)=Q*(Z+W)
      DO 110 I=K1,M
        Q=dcmplx(zero,zero)
        DO 90 J=K1,N
90        Q=Q+dCONJG(A(K,J))*A(I,J)
        Q=Q/(Z*(Z+W))
        DO 100 J=K1,N
100       A(I,J)=A(I,J)-Q*A(K,J)
110     CONTINUE
C   Phase transformation
      Q=-dCONJG(A(K,K1))/CdABS(A(K,K1))
      DO 120 I=K1,M
120     A(I,K1)=A(I,K1)*Q
130   K=K1
      GO TO 10
C   Tolerance for negligible elements
140   EPS=zero
      DO 150 K=1,N
        S(K)=B(K)
        T(K)=C(K)
150   EPS=DMAX1(EPS,S(K)+T(K))
      EPS=EPS*ETA
C   Initialization of U and V
      IF (NU .EQ. 0)  GO TO 180
      DO 170 J=1,NU
        DO 160 I=1,M
160       U(I,J)=dcmplx(zero,zero)
170     U(J,J)=dcmplx(one,zero)
180   IF (NV .EQ. 0)  GO TO 210
      DO 200 J=1,NV
        DO 190 I=1,N
190       V(I,J)=dcmplx(zero,zero)
200     V(J,J)=dcmplx(one,zero)
C   QR diagonalization
210   DO 380 KK=1,N
        K=N1-KK
C   Test for split
220     DO 230 LL=1,K
          L=K+1-LL
          IF (ABS(T(L)) .LE. EPS)  GO TO 290
          IF (ABS(S(L-1)) .LE. EPS)  GO TO 240
230       CONTINUE
C   Cancellation of B(L)
240     CS=zero
        SN=one
        L1=L-1
        DO 280 I=L,K
          F=SN*T(I)
          T(I)=CS*T(I)
          IF (ABS(F) .LE. EPS)  GO TO 290
          H=S(I)
          W=SQRT(F*F+H*H)
          S(I)=W
          CS=H/W
          SN=-F/W
          IF (NU .EQ. 0)  GO TO 260
          DO 250 J=1,N
            X=dreal(U(J,L1))
            Y=dreal(U(J,I))
            U(J,L1)=dCMPLX(X*CS+Y*SN,0.)
250         U(J,I)=dCMPLX(Y*CS-X*SN,0.)
260       IF (NP .EQ. N)  GO TO 280
          DO 270 J=N1,NP
            Q=A(L1,J)
            R=A(I,J)
            A(L1,J)=Q*CS+R*SN
270         A(I,J)=R*CS-Q*SN
280       CONTINUE
C   Test for convergence
290     W=S(K)
        IF (L .EQ. K)  GO TO 360
C   Origin shift
        X=S(L)
        Y=S(K-1)
        G=T(K-1)
        H=T(K)
        F=((Y-W)*(Y+W)+(G-H)*(G+H))/(2.d0*H*Y)
        G=SQRT(F*F+one)
        IF (F .LT. zero)  G=-G
        F=((X-W)*(X+W)+(Y/(F+G)-H)*H)/X
C   QR step
        CS=one
        SN=one
        L1=L+1
        DO 350 I=L1,K
          G=T(I)
          Y=S(I)
          H=SN*G
          G=CS*G
          W=SQRT(H*H+F*F)
          T(I-1)=W
          CS=F/W
          SN=H/W
          F=X*CS+G*SN
          G=G*CS-X*SN
          H=Y*SN
          Y=Y*CS
          IF (NV .EQ. 0)  GO TO 310
          DO 300 J=1,N
            X=dreal(V(J,I-1))
            W=dreal(V(J,I))
            V(J,I-1)=dCMPLX(X*CS+W*SN,0.)
300         V(J,I)=dCMPLX(W*CS-X*SN,0.)
310       W=SQRT(H*H+F*F)
          S(I-1)=W
          CS=F/W
          SN=H/W
          F=CS*G+SN*Y
          X=CS*Y-SN*G
          IF (NU .EQ. 0)  GO TO 330
          DO 320 J=1,N
            Y=dreal(U(J,I-1))
            W=dreal(U(J,I))
            U(J,I-1)=dCMPLX(Y*CS+W*SN,0.)
320         U(J,I)=dCMPLX(W*CS-Y*SN,0.)
330       IF (N .EQ. NP)  GO TO 350
          DO 340 J=N1,NP
            Q=A(I-1,J)
            R=A(I,J)
            A(I-1,J)=Q*CS+R*SN
340         A(I,J)=R*CS-Q*SN
350       CONTINUE
        T(L)=zero
        T(K)=F
        S(K)=X
        GO TO 220
C   Convergence
360     IF (W .GE. zero)  GO TO 380
        S(K)=-W
        IF (NV .EQ. 0)  GO TO 380
        DO 370 J=1,N
370       V(J,K)=-V(J,K)
380     CONTINUE
C   Sort singular values
      DO 450 K=1,N
        G=-one
        J=K
        DO 390 I=K,N
          IF (S(I) .LE. G)  GO TO 390
          G=S(I)
          J=I
390       CONTINUE
        IF (J .EQ. K)  GO TO 450
        S(J)=S(K)
        S(K)=G
        IF (NV .EQ. 0)  GO TO 410
        DO 400 I=1,N
          Q=V(I,J)
          V(I,J)=V(I,K)
400       V(I,K)=Q
410     IF (NU .EQ. 0)  GO TO 430
        DO 420 I=1,N
          Q=U(I,J)
          U(I,J)=U(I,K)
420       U(I,K)=Q
430     IF (N .EQ. NP)  GO TO 450
        DO 440 I=N1,NP
          Q=A(J,I)
          A(J,I)=A(K,I)
440       A(K,I)=Q
450     CONTINUE
C   Back transformation
      IF (NU .EQ. 0)  GO TO 510
      DO 500 KK=1,N
        K=N1-KK
        IF (B(K) .EQ. zero)  GO TO 500
        Q=-A(K,K)/CdABS(A(K,K))
        DO 460 J=1,NU
460       U(K,J)=Q*U(K,J)
        DO 490 J=1,NU
          Q=dcmplx(zero,zero)
          DO 470 I=K,M
470         Q=Q+dCONJG(A(I,K))*U(I,J)
          Q=Q/(CdABS(A(K,K))*B(K))
          DO 480 I=K,M
480         U(I,J)=U(I,J)-Q*A(I,K)
490       CONTINUE
500     CONTINUE
510   IF (NV .EQ. 0)  GO TO 570
      IF (N .LT. 2)  GO TO 570
      DO 560 KK=2,N
        K=N1-KK
        K1=K+1
        IF (C(K1) .EQ. zero)  GO TO 560
        Q=-dCONJG(A(K,K1))/CdABS(A(K,K1))
        DO 520 J=1,NV
520       V(K1,J)=Q*V(K1,J)
        DO 550 J=1,NV
          Q=dcmplx(zero,zero)
          DO 530 I=K1,N
530         Q=Q+A(K,I)*V(I,J)
          Q=Q/(CdABS(A(K,K1))*C(K1))
          DO 540 I=K1,N
540         V(I,J)=V(I,J)-Q*dCONJG(A(K,I))
550       CONTINUE
560     CONTINUE
570   RETURN
      END
