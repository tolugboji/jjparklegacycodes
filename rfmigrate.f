c  program rfmigrate
c  9/20/00 JJP -- adapted from recfunk  --- UPDATED 08/19/07 JJP
c
c  migrates MTC receiver functions in the frequency domain.
c  requires a stacking model in the anirec format
c  such a model may have anisotropy parameters, 
c  but migration code only uses the isotropic velocities. 
c
c  has kluge to cheat the pre-event noise for synthetic records  3/12/00 JJP
c  check to see if the kluge is commented out
c
c  this version of the RF code reads a file of data filenames 
c  you have two choices: either read the time intervals in the filename file
c  or read them in the sac header
c  the data must be binary SAC format
c  horizontals must be rotated to radial and transverse
c
c  for start times in the file:
c  the file is "in_recfunk" and has lines of the form:
c
c  1997.070.19.33.bh?  <-- code replaces ? with r,t,z
c  57 52 	       <-- start time of analysis window, window duration (sec)
c  1997.076.08.15.bh?
c  62 62 
c  ...
c  ...
c  ...
c  stop                <-- tells code that data is finished, 799 events max
c
c
c  for start times in the SAC header
c  reads seismic record start times from the sac header
c  will search the SAC header for specific markers of P phases
c  T1 - P, Pdiff    ahead(12)
c  T2 - PKP,PKIKP   ahead(13)
c  T3 - PP          ahead(14)
c  T1=T2=T3=0 ==> use original A-marker ahead(9)
c
c   code does NOT combine data with different sample rates
c  data files limited to 99K pnts. To increase, see common block /datastuff/
c
c   many intermediate quantities are plotted with PLOTIT as the code proceeds.
c  other intermediate quantities can be plotted by uncommenting calls to PLOTIT
c
c  the code writes the BAZ- and EPICEN-dependent RFs to files
c  in a format easily digested by GMT (traces are separated by ">" lines)
c
c  filenames: out[rt]_baz.grid oum[rt]_epi.grid oum[rt]_baz.grid
c
c  these files are overwritten everytime you run the program
c  so rename them to save them
c
c  xf77 -o /park/backup/bin/rfmigrate rfmigrate.f /park/backup/Plotxy/plotlib.a /park/backup/Ritz/eislib.a /park/backup/Ritz/jlib.a
c  xf77 -o /Users/jjpark/bin/rfmigrate rfmigrate.f /Users/jjpark/Plotxy/plotlib.a /Users/jjpark/Ritz/eislib.a /Users/jjpark/Ritz/jlib.a
c
      implicit real*4(a-h,o-z)
      implicit integer*4(i-n)
      real*8 ar,ai,el(12),aaa
      complex*8 zc,zero
      complex*8 afft,rf,rfs
      character*80 name,subtext,string,name_stack
      character*28 title(2)
      character*10 cmps(3)
      character*18 xlabel(2)
      character*14 xlabel2(2)
      character*12 xlabel3(2),rfout(2),rfout1(2),rfout2(2)
      character comp(3)
      common/nnaamm/iwflag
      common/npiprol/anpi
      common/stap2/tap(16400,16)
      common/taperz/ta(16400,8,3),tai(16400,8,3)
      common/stap0/ar(16400),ai(16400),bb(16400),time(16400)
      common/specs/afft(4100,8,3),rf(4100,2),sss(4100,6),crf(4100,2)
      common/saveit/rft(4100,699,2),crft(4100,2),rfs(4100,699,2),
     x   drfs(4100,699,2),bazs(699),gcarc(699),iflag(699),
     x                         epic(699),s2n(699),ip(699)
      common/datastuff/a(99000,3),pdat(16400),tdat(16400),drf(4100,2)
c NOTE THAT MODEL PARAMETERS ARE REAL*4 IN THIS CODE, NOT REAL*8 AS IN ANIREC
c ONLY 20 LAYERS OVER HALFSPACE ARE ALLOWED
      common/model/z(21),dz(21),rho(21),vp(21),vs(21),vp2(21),
     x               vp4(21),vs2(21),vss(21)
      common/model2/xmu(21),xla(21),xmu2(21),xla2(21),xla4(21)
      common/modstk/tau(21),itau(21),psd(21),gam(21),xik(21)
      common/migstk/arm(4100,2,21),aim(4100,2,21),dam(4100,2,21)
      common/tt_p/d_p(200),d_pkp(200),s_p(200),s_pkp(200),n_p,n_pkp
      common/header/ahead(158)
      common/distrib/xi(72,36),yi(72,36),sumij(72,36)
      common/chisq/chi(160000),enchi(160000),bbaz(160000),
     x chim(200,2),enchim(200,2)
      dimension iah(158),chead(158),mmonth(12),fr(3),tt(3),ttt(3),
     x  xx(2),yy(2)
      equivalence (ahead,iah),(ahead,chead)
      data mmonth/0,31,59,90,120,151,181,212,243,273,304,334/
      data comp/'r','t','z'/
      data cmps/'Radial    ','Transverse','Vertical  '/
      data title/'Radial Receiver Function    ',
     x 'Transverse Receiver Function'/
      data xlabel/'H\\sub{R}(f) phase','H\\sub{T}(f) phase'/
      data xlabel2/'|H\\sub{R}(f)|','|H\\sub{T}(f)|'/
      data xlabel3/'H\\sub{R}(t)','H\\sub{T}(t)'/
      data rfout/'rfout_radial','rfout_transv'/
      data rfout1/'rfoua_radial','rfoua_transv'/
      data rfout2/'rfoup_radial','rfoup_transv'/
      pi=3.14159265358979
      con=180./pi
      pih=pi/2.
      zero=cmplx(0.,0.)
      fmax=5.
      print *,'enter velocity model for stacking', 
     x ' <space-ret>=stack_model'
      read(5,101) name_stack
      if(name_stack(1:1).eq.' ') then
        open(7,file='stack_model',form='formatted')
      else
        open(7,file=name_stack,form='formatted')
      endif
      read(7,101) subtext
      print *,subtext
      read(7,*) nl
      if(nl.gt.20) then
        print *,'only twenty layers in stack model are allowed!'
        stop
      endif
c  read in theta,phi in degrees - polar coords of fast axis
      nlp=nl+1
      nlm=nl-1
      do i=1,nlp
        read(7,*) theta,phi
c  read depth to ith interface, vp (m/sec), pk-to-pk cos(2th) relative P pert
c  pk-to-pk cos(4th) relative P pert, v_s,  pk-to-pk cos(2th) relative S pert
c  density (kg/m**3)
        read(7,*) z(i),vp(i),vp2(i),vp4(i),vs(i),vs2(i),rho(i)
c  recall that we interpret fractional values of b,c,e 
c  as peak-to-peak relative velocity perts.
c  therefore, e=0.02 is 2% pert to mu from slowest to fastest
c  DO NOT NORMALIZE THE MODEL
        xmu(i)=rho(i)*vs(i)**2
        xmu2(i)=vs2(i)*xmu(i)
        xla(i)=rho(i)*vp(i)**2
        xla2(i)=vp2(i)*xla(i)
        xla4(i)=vp4(i)*xla(i)
        vs(i)=vs(i)
        vp(i)=vp(i)
        rho(i)=rho(i)
        z(i)=z(i)
      end do
      close(7)
      do i=2,nl
        dz(i)=z(i)-z(i-1)
      end do
      dz(1)=z(1)
      call read_ttimes
      print *,'enter fmax (Hz)'
      read(5,*) fmax
c  some default values
      anpi=2.5
      nwin=3
      npad=16384
      nnf=npad/2
      do i=1,4100
        crft(i,1)=0.
        crft(i,2)=0.
      end do
      irec=0
 1010 print *,'time intervals in file (0) or in SAC-header (1)'
      read(5,*) isegment
      if(isegment.eq.0) then
        print *,
     x'file of seismic records to read? (space-ret: in_recfunk)'
        read(5,101) name
        if(name(1:1).eq.' ') then
          open(10,file='in_recfunk',form='formatted')
        else
          open(10,file=name,form='formatted')
        endif
      elseif(isegment.eq.1) then
        print *,
     x'file of seismic records to read? (space-ret: in_recpick)'
        read(5,101) name
        if(name(1:1).eq.' ') then
          open(10,file='in_recpick',form='formatted')
        else
          open(10,file=name,form='formatted')
        endif
	print *,'duration of data windows for analysis'
	read(5,*) postwin
      else
        go to 1010
      endif
      print *,'minimum number of events in a binsum (eg 1 or 2)'
      read(5,*) ibskip
      print *,'rotate to LQT coordinates to isolate P and SV? (0=no)'
      read(5,*) lqt
   10 print *,'enter filename e.g. 1998.361.bhz'
      read(10,101) name
  101 format(a)
      if(name(1:4).eq.'stop') go to 111
c  we assume that the last character is either 'r','t',or 'z'
      do i=80,1,-1
        if(name(i:i).ne.' ') then
          namlen=i
          go to 98
        endif
      end do
      print *,name
      print *,'no filename?'
      stop
   98 continue
      do kcmp=1,3
        name(namlen:namlen)=comp(kcmp)
        print *,name
        call sacread(name,a(1,kcmp),ierr)
        dt=ahead(1) 
        tt(1)=0.
        tt(2)=dt
        tt(3)=dt
        nscan=iah(80)
        if(nscan.gt.99000) then
          print *,'CAREFUL! Data length is greater than 99000:', nscan
          pause
        endif
      end do
      if(isegment.eq.0) then
        print *,'P window start time, duration (sec)'
        read(10,*) pstart,postwin
	imark=0
      else
c   READING TIME INTERVAL FROM SAC HEADER
c  ahead(12) is SAC's T1 parameter, for arrival time.  Set by pressing T & 1 in plotpk
        if(ahead(12).gt.1) then
          tmark=ahead(12)
	  imark=1
c  ahead(13) is SAC's T2 parameter, for arrival time.  Set by pressing T & 2 in plotpk
        elseif(ahead(13).gt.1) then
          tmark=ahead(13)
	  imark=2
c  ahead(14) is SAC's T3 parameter, for arrival time.  Set by pressing T & 3 in plotpk
        elseif(ahead(14).gt.1) then
          tmark=ahead(14)
	  imark=3
c  ahead(9) is SAC's A parameter, for arrival time.  Set by pressing A in plotpk
        elseif(ahead(9).gt.1) then
          tmark=ahead(9)
	  imark=0
        else
          print *,' hey! no P-wave mark for file ',name
	  stop
        endif
        pstart=tmark-ahead(6)-3.
        print *,'sac params b and tmark, pstart',ahead(6),tmark,pstart
      endif
      irec=irec+1
      baz=ahead(53)
      bazs(irec)=ahead(53)
      gcarc(irec)=ahead(54)
      epic(irec)=ahead(54)
      print *,bazs(irec),'= back azim ',ahead(54),'= epicentral dist'
      if(ahead(53).lt.-0.1.or.ahead(53).gt.360.1) then
        print *,'HEY! THE BACK AZIMUTH OF THIS RECORD IS OUT OF RANGE!'
        stop
      elseif(ahead(54).lt.-0.1.or.ahead(54).gt.180.1) then
        print *,'HEY! THE GCARC OF THIS RECORD IS OUT OF RANGE!'
        stop
      endif
c  ddf is cycles/day
      ddf=1./(dt*npad)
      nf=fmax/ddf+1
      if(nf.gt.4100) then
        print *,'nf is too big', nf
	stop
      endif
      fr(1)=0.
      fr(2)=ddf
      fr(3)=ddf
      npts=postwin/dt
      nst=pstart/dt
      call taper(npts,nwin,el)
      print *,'eigenvalues ',(el(k),k=1,nwin)
c  loop over components
      print *,'npad,nf,nst,npts',npad,nf,nst,npts
      print *,'dt,nscan,ddf',dt,nscan,ddf
c      pause
      npts0=min0(nst+1,npts)
      if(npts0.lt.npts) then
        call taper(npts0,nwin,el)
        print *,'eigenvalues ',(el(k),k=1,nwin)
      endif
      epipi=epic(irec)
c  preferred option -- use parameterized relation between GCARC and zrrot
c  compute approximate angle between Z and R components for P wave
c  if the phase is PP, its moveout is the same as a P wave at gcarc/2
      iflag(irec)=1
      if(imark.eq.3) then
	epipi=epipi/2.
	gcarc(irec)=epipi
      endif
      if(imark.eq.2) then
	zrrot=9.0
	iflag(irec)=2
      else
        if(epipi.lt.80.) then
          zrrot=42.+(20.-epipi)*(20./60.)
        elseif(epipi.lt.118.) then
          zrrot=16.+(6./900.)*(110.-epipi)**2
        elseif(imark.eq.0) then
          zrrot=9.0
	  iflag(irec)=2
	else
          zrrot=16.+(6./900.)*(110.-epipi)**2
	endif
      endif
c  here is the option to rotate the Radial and vertical Components to
c  a P and SV coordinate system    
      if(lqt.ne.0) then
        zrrot=zrrot/con
        cs=cos(zrrot)
        sn=sin(zrrot)
c  rotate from Z-R coords to L-Q, similar to P-SV
        do i=1,nst+npts
          plog=cs*a(i,3)+sn*a(i,1)
	  a(i,1)=cs*a(i,1)-sn*a(i,3)
   	  a(i,3)=plog
        end do
      endif
      do kcmp=1,3
c  first: compute spectrum of pre-event noise
        do i=1,nf
          sss(i,kcmp)=0.
        end do
        do k=1,nwin
          do i=1,npts0
c OK to reverse order of data here, as we only use mod-sq spectrum
            ar(i)=a(nst+1-i,kcmp)*tap(i,k)
            ai(i)=0.d0
          end do
          do i=npts0+1,npad
            ar(i)=0.d0
            ai(i)=0.d0
          end do   
c  we use the complex FFT routine fft2 -- uses real*8 input arrays
c      print *,'we use the complex FFT routine fft2'
          call fft2(ar,ai,npad)
          do i=1,nf
            sss(i,kcmp)=sss(i,kcmp)+(ar(i)**2+ai(i)**2)
          end do
        end do
c  if pre-event noise duration is truncated, boost the noise spectrum by
c  relative length factor
        if(npts0.lt.npts) then
          do i=1,nf
            sss(i,kcmp)=sss(i,kcmp)*float(npts)/float(npts0)
          end do
        endif
      end do
      if(npts0.lt.npts) then
        call taper(npts,nwin,el)
        print *,'eigenvalues ',(el(k),k=1,nwin)
      endif
      do kcmp=1,3
        do i=1,nf
          sss(i,kcmp+3)=0.
        end do
c  first: compute spectrum of P coda
        do k=1,nwin
          do i=1,npts
            ar(i)=a(i+nst,kcmp)*tap(i,k)
            bb(i)=a(i+nst,kcmp)*tap(i,k)
            ai(i)=0.d0
          end do
c          call plotit(tt,bb,dum,npts,'tapered data',
c     x     'time',' ',1,0,0.0,0,1)
          do i=npts+1,npad
            ar(i)=0.d0
            ai(i)=0.d0
          end do
c  we use the complex FFT routine fft2 -- uses real*8 input arrays
c      print *,'we use the complex FFT routine fft2'
          call fft2(ar,ai,npad)
          do i=1,nf
            afft(i,k,kcmp)=dcmplx(ar(i),ai(i))
            sss(i,kcmp+3)=sss(i,kcmp+3)+(ar(i)**2+ai(i)**2)
          end do
        end do
      end do
c  simple correlation coefficient with vertical (kcmp=3)
      do l=1,2
        do n=1,nf
          zc=zero
          do k=1,nwin
            zc=zc+conjg(afft(n,k,3))*afft(n,k,l)
          end do
          rf(n,l)=zc/(sss(n,6)+sss(n,3))
          crf(n,l)=cabs(zc)**2/(sss(n,6)*sss(n,3+l))
          drf(n,l)=sqrt((1.0-crf(n,l))/(crf(n,l)*(nwin-1)))
     x                                         *cabs(rf(n,l))
          crft(n,l)=crft(n,l)+crf(n,l)
c          print *,n,l,zc,aaa,rf(n,l)
          rfs(n,irec,l)=rf(n,l)
          drfs(n,irec,l)=drf(n,l)
        end do
      end do
      sq2=sqrt(2.)
c      if(isss.ne.12345) go to 787
c      print *,isss
c      pause
      do l=1,2
        do n=1,nf
c	  if(n.ge.nf/2) then
c            fac=cos(pi*(n-nf/2)/nf)**2
c	  else
c            fac=1.0
c	  endif
          fac=cos(pi*(n-1)/(2*nf))**2
          ar(n)=real(rf(n,l))*fac
          ar(npad+2-n)=real(rf(n,l))*fac
          ai(n)=-imag(rf(n,l))*fac
          ai(npad+2-n)=imag(rf(n,l))*fac
          pdat(n)=drf(n,l)*fac/sq2
          tdat(n)=fr(1)+(n-1)*fr(2)
        end do
        do n=nf+1,npad/2+2
          ar(n)=0.d0
          ar(npad+2-n)=0.d0
          ai(n)=0.d0
          ai(npad+2-n)=0.d0
        end do
        dtdat=(tdat(nf)-tdat(1))/30.
        tdata=tdat(1)-dtdat
        tdatb=tdat(nf)+dtdat
        bbmx=0.
        do n=1,npad
          bb(n)=dsqrt(ar(n)**2+ai(n)**2)
          bbmx=amax1(bbmx,bb(n))
        end do
        call plotit_axes(0.,0.,0.,0.)
        do n=1,npad
          if(bb(n).gt.1e-4) then
            pdat(n)=con*(pdat(n)/bb(n)) 
          else
            pdat(n)=360.
          endif
          bb(n)=con*datan2(ai(n),ar(n))
        end do
        call plotit_axes(0.,0.,0.,0.)
c  invert the Fourier transform
        call fft2(ar,ai,npad)
c  normalization factor:
c  divide by npad for fft2 routine normalization
c  mult by nnf/nf to compensate for losing high freqs
c  mult by 2 if cosine**2 taper is applied to spectral RF
        fac=2.*float(nnf)/(float(npad)*float(nf))
c        fac=(4./3.)*float(nnf)/(float(npad)*float(nf))
c        fac=float(nnf)/(float(npad)*float(nf))
        print *,'ddf,npad,nnf,nf,fac',ddf,npad,nnf,nf,fac
        nmm=30./dt
        nnm=nmm+200
        do i=1,nmm
          bb(200+i)=ar(i)*fac
          time(200+i)=(i-1)*dt
        end do
        do i=1,200
          bb(201-i)=ar(npad+1-i)*fac
          time(201-i)=-i*dt
        end do
        ttt(1)=-200*dt
        ttt(2)=dt
        ttt(3)=dt
        do i=1,nnm 
          rft(i,irec,l)=baz+50.*bb(i)
        end do
c        call plotit(ttt,bb,dum,400,'magnified version',
c     x    'time(sec)','H(t)',1,0,0.0,0,59+3*l)
        nmpt=40./dt
c        call plotit(ttt,bb,dum,nnm,title(l),
c     x    'time(sec)',xlabel3(l),1,0,0.0,0,60+3*l)
c        open(12,file=rfout(l),form='formatted')
c        write(12,1077) (time(i),bb(i),i=1,nnm)
c        close(12)
      end do
  787 continue
 1076 format(3f10.4)
 1077 format(2f10.4)
      go to 10
  111 continue
      close(10)
c  plot the inferred angle between radial and vertical against epicentral dist
c      do i=1,irec
c        if(epic(i).lt.80.) then
c	  bb(i)=42.+(20.-epic(i))*(20./60.)
c	elseif(epic(i).lt.118.) then
c	  bb(i)=16.+(6./900.)*(110.-epic(i))**2
c	else
c	  bb(i)=9.0
c	endif
c      end do
c      call plotit(epic,bb,dum,irec,' ',' ',' ',2,0,0.0,0,0)
c      call plotit(epic,bbaz,dum,irec,'P-SV Rotation Check',
c     x 'Epicentral Distance','Inferred R/Z Angle',2,0,0.1,3,1)
c  average coherence as function of frequency
      do l=1,2
        do i=1,nf
          crft(i,l)=crft(i,l)/irec
        end do
      end do
      print *,irec
      call plotit_axes(tdata,tdatb,0.0,1.0)
      bb(1)=0.
      bb(2)=fmax
      bb(3)=1./float(nwin)
      bb(4)=bb(3)
      call plotit(bb,bb(3),dum,2,' ',' ',' ',2,0,0.0,0,0)
      call plotit(fr,crft(1,2),dum,nf,'Average P-Coda Coherence',
     x    'Freq(Hz)','Avg C\\sup{2}(f)',1,0,0.05,0,0)
      call plotit(fr,crft(1,1),dum,nf,'Average P-Coda Coherence',
     x    'Freq(Hz)','Avg C\\sup{2}(f)',1,0,0.0,0,1)
      print *,irec
      do i=1,nmpt
        time(i)=-ttt(1)-(i-1)*ttt(2)
      end do
      nnn=25./dt
      print *,irec
      kaz=0
      open(12,file='outr_baz.grid',form='formatted')
      open(13,file='outt_baz.grid',form='formatted')
      bazmax=355.
      bazmin=0.
      bazinc=-5.
      print *,'back-azimuth range to plot:'
      print *,bazmax, bazmin, bazinc
      print *,'change baz-interval or baz-spacing? (1=yes)'
      read(5,*) ickbaz
      if(ickbaz.eq.1) then
        print *,'enter back-azimuth range:'
        print *,'bazmax, bazmin, bazinc'
        print *,'will make bazinc negative to step back thru baz values'
        read (5,*) bazmax, bazmin, bazinc
        if((bazmax-bazmin)*bazinc.gt.0) bazinc=-bazinc
      endif
      abazinc=abs(bazinc)
      do baz=bazmax, bazmin, bazinc
        do l=1,2
          do n=1,nf
            rf(n,l)=zero
            drf(n,l)=0.
          end do    
          jrec=0    
          do i=1,irec
            test=abs(baz-bazs(i))
            if(test.lt.abazinc.or.test.gt.360.-abazinc) then
              jrec=jrec+1
              do n=1,nf
                sig=drfs(n,i,l)
                rf(n,l)=rf(n,l)+rfs(n,i,l)/sig
                drf(n,l)=drf(n,l)+1.0/sig
              end do  
            endif
          end do
c          print *,'# of records in bin:',jrec
          if(jrec.ge.ibskip) then
            kaz=kaz+1
c  do the expectation of variance for the weighted sum yourself
c  if you doubt the formula here -- basically the weighted terms in rf-sum
c  have unit variance, so that variance of the total variance is jrec/drf**2
            do n=1,nf
c	      if(n.ge.nf/2) then
c                fac=cos(pi*(n-nf/2)/nf)**2
c	      else
c                fac=1.0
c	      endif
              fac=cos(pi*(n-1)/(2*nf))**2
              rf(n,l)=rf(n,l)/drf(n,l)
              ar(n)=fac*real(rf(n,l))
              ai(n)=-fac*imag(rf(n,l))
              ar(npad+2-n)=fac*real(rf(n,l))
              ai(npad+2-n)=fac*imag(rf(n,l))
              drf(n,l)=fac*sqrt(float(jrec))/drf(n,l)
            end do
            do n=nf+1,npad-nf+1
              ar(n)=0.d0
              ai(n)=0.d0
            end do
            write(string,109) baz
            bbmx=0.
            do n=1,nf
              bb(n)=dsqrt(ar(n)**2+ai(n)**2)
              bbmx=amax1(bbmx,bb(n))
              pdat(n)=drf(n,l)/sq2
            end do
            do n=1,npad
              if(bb(n).gt.1e-4) then
                pdat(n)=con*(pdat(n)/bb(n)) 
              else
                pdat(n)=360.
              endif
              bb(n)=con*datan2(ai(n),ar(n))
            end do
c            call plotit_axes(tdata,tdatb,-180.,180.)
c            call plotit(tdat,bb,pdat,nf,'Frequency Domain',
c     x   'frequency (Hz)',xlabel(l),3,0,0.05,0,59+l*3)
            do n=1,nf
              bb(n)=ar(n)
            end do
            call fft2(ar,ai,npad)
c  normalization factor:
c  divide by npad for fft2 routine normalization
c  mult by nnf/nf to compensate for losing high freqs
c  mult by 2 if cosine**2 taper is applied to spectral RF
            fac=2.*float(nnf)/(float(npad)*float(nf))
c            fac=(4./3.)*float(nnf)/(float(npad)*float(nf))
            do i=1,nmm
              bb(200+i)=ar(i)*fac
            end do
            do i=1,200
              bb(201-i)=ar(npad+1-i)*fac
            end do
            ttt(1)=-200*dt
            ttt(2)=dt
            ttt(3)=dt
            kz=(kaz-l)/2+1
            do i=1,nnm 
              rft(i,kz,l)=baz+50.*bb(i)*l
              t3=-time(i)
              iunit=11+l
              write(iunit,1022) t3,baz,bb(i)
            end do
            write(iunit,101) '>'
          endif
        end do
 1022 format(f7.3,f6.1,f7.3)
  102 format(a)
      end do  
      close(12)
      close(13)
      kaz=kaz/2
      print *,kaz,' traces'   
      do iagain=1,2
      call plotit_axes(0.,0.,0.,0.)
      do n=1,kaz
        call plotit(rft(150,n,2),time(150),dum,nnn,
     x   'Composite Transverse RF Section',
     x   'Back-Azimuth (degrees CW from N)',
     x  'time(sec)',2,0,0.0,0,21*(n/kaz))
      end do
            
      call plotit_axes(0.,0.,0.,0.)
      do n=1,kaz
        call plotit(rft(150,n,1),time(150),dum,nnn,
     x   'Composite Radial RF Section',
     x   'Back-Azimuth (degrees CW from N)',
     x  'time(sec)',2,0,0.0,0,22*(n/kaz))
      end do
      end do
  109 format('Back Azimuth Centered on ',f4.0,'degrees')
c  OK, LETS TRY THE SAME THING, BUT WITH VELOCITY MIGRATION
      sdelay=0.
      pdelay=0.
      do i=1,nl
        sdelay=sdelay+(dz(i)/vs(i))
        pdelay=pdelay+(dz(i)/vp(i))
        tau(i)=sdelay-pdelay
        itau(i)=tau(i)/dt
        print *,itau(i),tau(i)
      end do
      itau(nlp)=4100
      print *, 'organ-pipe mode count at 1 Hz in stack of layers: S & P'
      print *,'These are the S & P traveltimes from the basal interface'
      print 104,sdelay,pdelay
  104 format(2f10.1)
      open(12,file='oumr_baz.grid',form='formatted')
      open(13,file='oumt_baz.grid',form='formatted')

      kaz=0
      do baz=bazmax, bazmin, bazinc
        do l=1,2
          do k=1,nlp
            do n=1,nf
              arm(n,l,k)=0.
              aim(n,l,k)=0.
              dam(n,l,k)=0.
            end do    
          end do    
          jrec=0    
          do i=1,irec
            test=abs(baz-bazs(i))
            if(test.lt.abazinc.or.test.gt.360.-abazinc) then
c            if(test.lt.20..or.test.gt.340.) then
              jrec=jrec+1
c  get p-slowness -- iflag branches on the star	t time of the analysis window
              igcarc=gcarc(i)
              fac=gcarc(i)-igcarc
              print *,'igcarc,i,gcarc(i),fac',igcarc,i,gcarc(i),fac
              if(iflag(i).eq.1) then		! P-phase
                if(igcarc.ge.180.or.igcarc.le.0) then
                  print *,'WHOA! igcarc,gcarc = ',igcarc,gcarc(i)
                  pause
                endif
c  allow extreme P_diff phases (moveout is constant for P_diff)
                if(igcarc.gt.129) igcarc=129 
                slow=(1.-fac)*s_p(igcarc)+fac*s_p(igcarc+1)
              elseif(iflag(i).eq.2) then	! PKP-phase
                if(igcarc.ge.180.or.igcarc.lt.90) then
                  print *,'WHOA! igcarc,gcarc = ',igcarc,gcarc(i)
                  pause
                endif
                ig=igcarc-89
                slow=(1.-fac)*s_pkp(ig)+fac*s_pkp(ig+1)
              else
                print *,'WHOA! iflag(i),i = ',iflag(i),i
                pause
              endif
c  the stack-model velocities are in SI units, s_p is in sec/km
              slw=(slow/1000.)       
c  compute the slowness-dependent time delays at the interfaces of stack-model
              do k=1,nlp
                coss=sqrt(1.-vs(k)*vs(k)*slw*slw)
                cosp=sqrt(1.-vp(k)*vp(k)*slw*slw)
c  trapdoor to avoid incoming slownesses that are evanescent (e.g. head waves)
c  basically, one stacks with the last acceptable interval velocity
                if(vp(k)*slw.lt.1.) then
                  gam(k)=(vp(k)*coss-vs(k)*cosp)/(vp(k)-vs(k))
                else
                  gam(k)=gam(k-1)
                endif
              end do
              psd(1)=gam(1)*tau(1)
              xik(1)=0.
              do k=2,nlp
                psd(k)=psd(k-1)+gam(k)*(tau(k)-tau(k-1))
                xik(k)=psd(k-1)/gam(k)-tau(k-1)
              end do
c  spline-interpolate the RF in freq domain
              do n=1,nf
                sss(n,1)=real(rfs(n,i,l))
                sss(n,2)=imag(rfs(n,i,l))
                sss(n,3)=drfs(n,i,l)
              end do
              call splneq(nf,sss(1,1),sss(1,4))
              call splneq(nf,sss(1,2),sss(1,5))
              call splneq(nf,sss(1,3),sss(1,6))
c  sum into nlp RF stacks, each for a different layer of the stacking model
              do k=1,nlp
                dfm=ddf/gam(k)
                dxm=1.0/gam(k)
                dcs=cos(2.*pi*dfm*xik(k)) 
                dsn=sin(2.*pi*dfm*xik(k)) 
c  we start at f=0., so that the migration phase factor = 1
c  csm and snm are real/imag parts of the migration phase factor
c  and the position in the spline is x=1  (fm is "migration frequency")
                csm=1.
                snm=0.
                xm=1.
                do n=1,nf
                  sig=evaleq(xm,nf,sss(1,3),sss(1,6),0,1.)
                  dre=evaleq(xm,nf,sss(1,1),sss(1,4),0,1.)
                  dim=evaleq(xm,nf,sss(1,2),sss(1,5),0,1.)
                  dre=dre/(sig*gam(k))
                  dim=dim/(sig*gam(k))
                  arm(n,l,k)=arm(n,l,k)+dre*csm+dim*snm
                  aim(n,l,k)=aim(n,l,k)-dre*snm+dim*csm
                  dam(n,l,k)=dam(n,l,k)+1.0/sig
                  csm=csm*dcs-snm*dsn
                  snm=snm*dcs+csm*dsn
                  xm=xm+dxm
                end do
              end do  
            endif
          end do
          if(jrec.ge.ibskip) then
            kaz=kaz+1
c  do the expectation of variance for the weighted sum yourself
c  if you doubt the formula here -- basically the weighted terms in rf-sum
c  have unit variance, so that variance of the total variance is jrec/drf**2
c    start of k=1,nlp loop ccccccccc
            do k=1,nlp
cccccccccccccccccccccccccccccccccccc
              do n=1,nf
c	        if(n.ge.nf/2) then
c                  fac=cos(pi*(n-nf/2)/nf)**2
c	        else
c                  fac=1.0
c	        endif
                fac=cos(pi*(n-1)/(2*nf))**2
                arm(n,l,k)=arm(n,l,k)/dam(n,l,k)
                aim(n,l,k)=aim(n,l,k)/dam(n,l,k)
                ar(n)=fac*arm(n,l,k)
                ai(n)=-fac*aim(n,l,k)
                ar(npad+2-n)=fac*arm(n,l,k)
                ai(npad+2-n)=fac*aim(n,l,k)
                dam(n,l,k)=fac*sqrt(float(jrec))/dam(n,l,k)
              end do
              do n=nf+1,npad-nf+1
                ar(n)=0.d0
                ai(n)=0.d0
              end do
              write(string,109) baz
              bbmx=0.
              do n=1,nf
                bb(n)=dsqrt(ar(n)**2+ai(n)**2)
                bbmx=amax1(bbmx,bb(n))
                pdat(n)=dam(n,l,k)/sq2
              end do
              do n=1,npad
                if(bb(n).gt.1e-4) then
                  pdat(n)=con*(pdat(n)/bb(n)) 
                else
                  pdat(n)=360.
                endif
                bb(n)=con*datan2(ai(n),ar(n))
              end do
              do n=1,nf
                bb(n)=ar(n)
              end do
              call plotit_axes(0.,0.,0.,0.)
              call fft2(ar,ai,npad)
c  normalization factor:
c  divide by npad for fft2 routine normalization
c  mult by nnf/nf to compensate for losing high freqs
c  mult by 2 if cosine**2 taper is applied to spectral RF
              fac=2.*float(nnf)/(float(npad)*float(nf))
c              fac=(4./3.)*float(nnf)/(float(npad)*float(nf))
              do i=1,nmm
                bb(200+i)=ar(i)*fac
              end do
              do i=1,200
                bb(201-i)=ar(npad+1-i)*fac
              end do
              ttt(1)=-200*dt
              ttt(2)=dt
              ttt(3)=dt
              kz=(kaz-l)/2+1
c  we store nlp migrated receiver functions in rft(i,kz+k,l), k=1,nlp
              do i=1,nnm 
                rft(i,kz+k,l)=bb(i)
              end do
 1004 format(9g13.3)
c    end of k=1,nlp loop cccccccc
            end do   
ccccccccccccccccccccccccccccccccc  
            do i=1,nnm
              rft(i,kz,l)=0.
            end do         
            i1=1
            do k=1,nlp
              i2=min(nnm,200+itau(k))
              print *,i1,i2
              do i=i1,i2 
                rft(i,kz,l)=rft(i,kz,l)+rft(i,kz+k,l)
              end do
              i1=201+itau(k)
            end do
          endif
          print *,' l = ',l
        end do   ! l-loop over radial & transverse
c  Now thru the k=1,nlp loop, we assemble the segments of the migrated RF
c  the noncausal potion of the RF is copied from the shallow-layer migration 
        if(jrec.ge.ibskip) then
          call plotit_axes(0.,0.,0.,0.)
          xx(1)=tau(nlp)
          xx(2)=tau(nlp)
          yy(1)=-0.1
          yy(2)=0.1
          do i=1,nnm
            bb(i)=rft(i,kz,1)
            rft(i,kz,1)=baz+rft(i,kz,1)*50.
            t3=-time(i)
            write(12,1022) t3,baz,bb(i)
          end do
c  note that we boost the transverse RF by factor of 2 for plotting in 
c the interactive plotting routine plotit, but TRF
c  is written to disk without amplification
          do i=1,nnm
            bb(i)=rft(i,kz,2)
            rft(i,kz,2)=baz+rft(i,kz,2)*100.
            t3=-time(i)
            write(13,1022) t3,baz,bb(i)
          end do
          write(12,101) '>'
          write(13,101) '>'
        endif
      end do  
      close(12)
      close(13)

      kaz=kaz/2
      print *,kaz,' traces'   
      do iagain=1,2
      call plotit_axes(0.,0.,0.,0.)
      do n=1,kaz
        call plotit(rft(150,n,2),time(150),dum,nnn,
     x   'Composite Transverse RF Section',
     x   'Back-Azimuth (degrees CW from N)',
     x  'time(sec)',2,0,0.0,0,21*(n/kaz))
      end do
            
      call plotit_axes(0.,0.,0.,0.)
      do n=1,kaz
        call plotit(rft(150,n,1),time(150),dum,nnn,
     x   'Composite Radial RF Section',
     x   'Back-Azimuth (degrees CW from N)',
     x  'time(sec)',2,0,0.0,0,22*(n/kaz))
      end do
      end do
c  OK, LETS TRY THE SAME THING FOR THE EPICENTRAL SWEEP, 
c  BUT WITH VELOCITY MIGRATION  -- THIS FORMS A CHECK ON THE ALGORITHM
      open(12,file='oumr_epi.grid',form='formatted')
      open(13,file='oumt_epi.grid',form='formatted')
cc  NOW PLOT TRACES THAT ARE BINNED WITH EPICENTRAL DISTANCE & MiGRATED
      print *,'compute RFs binned with epicentral distance'
      print *,'enter back-azimuth limits ib1,ib2 (integers!)'
      print *,' ib1=ib2 -> 0,360 and 360-wraparound if ib1 > ib2 '
      read(5,*) ib1,ib2
      if(ib1.eq.ib2) then
        ick=1
        baz1=0.
        baz2=360.
      elseif(ib1.lt.ib2) then
        ick=1
        baz1=ib1
        baz2=ib2
      else
        ick=2
        baz1=ib1
        baz2=ib2
      endif
      kaz=0
c  code for changing the epicentral bin width
      ep1=0.
      ep2=175.
      dep=5.
      print *,'epicentral bins from',ep1,' to',ep2
      print *,'with halfbin width',dep
      print *,' do you want to change this spacing? (1=yes)'
      read(5,*) ickk
      if(ickk.eq.1) then
 2020   print *,'enter delta range and spacing (ep1,ep2,dep)'
        read(5,*) ep1,ep2,dep
        nep=(ep2-ep1)/dep+1
        if(nep.gt.500.or.nep.lt.1) then
          print *,'HEY! There are ',nep,' traces!'
          go to 2020
	endif
      endif
c  end code for changing the epicentral bin width
      do epi=ep1,ep2,dep
        do l=1,2
          do n=1,nf
            rf(n,l)=zero
            drf(n,l)=0.
          end do    
          do k=1,nlp
            do n=1,nf
              arm(n,l,k)=0.
              aim(n,l,k)=0.
              dam(n,l,k)=0.
            end do    
          end do    
          jrec=0    
          do i=1,irec
            test=abs(epi-gcarc(i))
            if(test.lt.dep) then
c  branch to limit the back-azimuth range for this sum
              ick1=0
              if(ick.eq.1) then
                if(bazs(i).ge.baz1.and.bazs(i).le.baz2) ick1=1
              else
                if(bazs(i).ge.baz1.or.bazs(i).le.baz2) ick1=1
              endif
              if(ick1.eq.1) then               
                jrec=jrec+1
c  get p-slowness -- iflag branches on the start time of the analysis window
                igcarc=gcarc(i)
                fac=gcarc(i)-igcarc
                print *,'igcarc,i,gcarc(i),fac',igcarc,i,gcarc(i),fac
                if(iflag(i).eq.1) then		! P-phase
                  if(igcarc.ge.180.or.igcarc.le.0) then
                    print *,'WHOA! igcarc,gcarc = ',igcarc,gcarc(i)
c                    pause
                  endif
                  slow=(1.-fac)*s_p(igcarc)+fac*s_p(igcarc+1)
                elseif(iflag(i).eq.2) then	! PKP-phase
                  if(igcarc.ge.180.or.igcarc.lt.90) then
                    print *,'WHOA! igcarc,gcarc = ',igcarc,gcarc(i)
c                    pause
                  endif
                  ig=igcarc-89
                  slow=(1.-fac)*s_pkp(ig)+fac*s_pkp(ig+1)
                else
                  print *,'WHOA! iflag(i),i = ',iflag(i),i
                  pause
                endif
c  the stac  k-model velocities are in SI units, s_p is in sec/km
                slw=(slow/1000.)       
c  compute   the slowness-dependent time delays at the interfaces of stack-model
                do k=1,nlp
                  coss=sqrt(1.-vs(k)*vs(k)*slw*slw)
                  cosp=sqrt(1.-vp(k)*vp(k)*slw*slw)
c  trapdoor to avoid incoming slownesses that are evanescent (e.g. head waves)
c  basically, one stacks with the last acceptable interval velocity
                  if(vp(k)*slw.lt.1.) then
                    gam(k)=(vp(k)*coss-vs(k)*cosp)/(vp(k)-vs(k))
                  else
                    gam(k)=gam(k-1)
                  endif
                end do
                psd(1)=gam(1)*tau(1)
                xik(1)=0.
                do k=2,nlp
                  psd(k)=psd(k-1)+gam(k)*(tau(k)-tau(k-1))
                  xik(k)=psd(k-1)/gam(k)-tau(k-1)
                end do
c  spline-interpolate the RF in freq domain
                do n=1,nf
                  sss(n,1)=real(rfs(n,i,l))
                  sss(n,2)=imag(rfs(n,i,l))
                  sss(n,3)=drfs(n,i,l)
                end do
                call splneq(nf,sss(1,1),sss(1,4))
                call splneq(nf,sss(1,2),sss(1,5))
                call splneq(nf,sss(1,3),sss(1,6))
c  sum into nlp RF stacks, each for a different layer of the stacking model
                do k=1,nlp
                  dfm=ddf/gam(k)
                  dxm=1.0/gam(k)
                  dcs=cos(2.*pi*dfm*xik(k)) 
                  dsn=sin(2.*pi*dfm*xik(k)) 
c  we start at f=0., so that the migration phase factor = 1
c  csm and snm are real/imag parts of the migration phase factor
c  and the position in the spline is x=1  (fm is "migration frequency")
                  csm=1.
                  snm=0.
                  xm=1.
                  do n=1,nf
                    sig=evaleq(xm,nf,sss(1,3),sss(1,6),0,1.)
                    dre=evaleq(xm,nf,sss(1,1),sss(1,4),0,1.)
                    dim=evaleq(xm,nf,sss(1,2),sss(1,5),0,1.)
                    dre=dre/(sig*gam(k))
                    dim=dim/(sig*gam(k))
                    arm(n,l,k)=arm(n,l,k)+dre*csm+dim*snm
                    aim(n,l,k)=aim(n,l,k)-dre*snm+dim*csm
                    dam(n,l,k)=dam(n,l,k)+1.0/sig
                    csm=csm*dcs-snm*dsn
                    snm=snm*dcs+csm*dsn
                    xm=xm+dxm
                  end do
                end do  
              endif
            endif
          end do
          if(jrec.ge.ibskip) then
            kaz=kaz+1
c  do the expectation of variance for the weighted sum yourself
c  if you doubt the formula here -- basically the weighted terms in rf-sum
c  have unit variance, so that variance of the total variance is jrec/drf**2
c    start of k=1,nlp loop ccccccccc
            do k=1,nlp
cccccccccccccccccccccccccccccccccccc
            do n=1,nf
c	      if(n.ge.nf/2) then
c                fac=cos(pi*(n-nf/2)/nf)**2
c	      else
c                fac=1.0
c	      endif
              fac=cos(pi*(n-1)/(2*nf))**2
              arm(n,l,k)=arm(n,l,k)/dam(n,l,k)
              aim(n,l,k)=aim(n,l,k)/dam(n,l,k)
              ar(n)=fac*arm(n,l,k)
              ai(n)=-fac*aim(n,l,k)
              ar(npad+2-n)=fac*arm(n,l,k)
              ai(npad+2-n)=fac*aim(n,l,k)
              dam(n,l,k)=fac*sqrt(float(jrec))/dam(n,l,k)
            end do
            do n=nf+1,npad-nf+1
              ar(n)=0.d0
              ai(n)=0.d0
            end do
            write(string,108) epi
            bbmx=0.
            do n=1,nf
              bb(n)=dsqrt(ar(n)**2+ai(n)**2)
              bbmx=amax1(bbmx,bb(n))
              pdat(n)=dam(n,l,k)/sq2
            end do
            do n=1,npad
              if(bb(n).gt.1e-4) then
                pdat(n)=con*(pdat(n)/bb(n)) 
              else
                pdat(n)=360.
              endif
              bb(n)=con*datan2(ai(n),ar(n))
            end do
            do n=1,nf
              bb(n)=ar(n)
            end do
            call plotit_axes(0.,0.,0.,0.)
            call fft2(ar,ai,npad)
c  normalization factor:
c  divide by npad for fft2 routine normalization
c  mult by nnf/nf to compensate for losing high freqs
c  mult by 2 if cosine**2 taper is applied to spectral RF
            fac=2.*float(nnf)/(float(npad)*float(nf))
c            fac=(4./3.)*float(nnf)/(float(npad)*float(nf))
            do i=1,nmm
              bb(200+i)=ar(i)*fac
            end do
            do i=1,200
              bb(201-i)=ar(npad+1-i)*fac
            end do
            ttt(1)=-200*dt
            ttt(2)=dt
            ttt(3)=dt
            kz=(kaz-l)/2+1
c  we store nlp migrated receiver functions in rft(i,kz+k,l), k=1,nlp
            do i=1,nnm 
              rft(i,kz+k,l)=bb(i)
            end do
c    end of k=1,nlp loop cccccccc
            end do   
ccccccccccccccccccccccccccccccccc  
            do i=1,nnm
              rft(i,kz,l)=0.
            end do         
            i1=1
            do k=1,nlp
              i2=min(nnm,200+itau(k))
              print *,i1,i2
              do i=i1,i2 
                rft(i,kz,l)=rft(i,kz,l)+rft(i,kz+k,l)
              end do
              i1=201+itau(k)
            end do
          endif
          print *,' l = ',l
        end do   ! l-loop over radial & transverse
c  Now thru the k=1,nlp loop, we assemble the segments of the migrated RF
c  the noncausal potion of the RF is copied from the shallow-layer migration 
        if(jrec.ge.ibskip) then
          call plotit_axes(0.,0.,0.,0.)
          xx(1)=tau(nlp)
          xx(2)=tau(nlp)
          yy(1)=-0.1
          yy(2)=0.1
          do i=1,nnm
            bb(i)=rft(i,kz,1)
            rft(i,kz,1)=epi+rft(i,kz,1)*50.
            t3=-time(i)
            write(12,1022) t3,epi,bb(i)
          end do
c  note that we boost the transverse RF by factor of 2 for PLOTIT display
          do i=1,nnm
            bb(i)=rft(i,kz,2)
            rft(i,kz,2)=epi+rft(i,kz,2)*100.
            t3=-time(i)
            write(13,1022) t3,epi,bb(i)
          end do
          write(12,101) '>'
          write(13,101) '>'
        endif
      end do  
      close(12)
      close(13)
      kaz=kaz/2
      print *,kaz,' traces'   
      print *,kaz,' traces'   
      call plotit_axes(0.,0.,0.,0.)
  108 format('Epicenters Centered on ',f4.0,'degrees')
      do n=1,kaz
        call plotit(rft(150,n,2),time(150),dum,nnn,
     x   'Composite Transverse RF Section',
     x   'Epicentral Distance (degrees)',
     x  'time(sec)',2,0,0.0,0,21*(n/kaz))
      end do
            
      call plotit_axes(0.,0.,0.,0.)
      do n=1,kaz
        call plotit(rft(150,n,1),time(150),dum,nnn,
     x   'Composite Radial RF Section',
     x   'Epicentral Distance (degrees)',
     x  'time(sec)',2,0,0.0,0,22*(n/kaz))
      end do

      stop
      end
c
      subroutine taper(n,nwin,el)
c
c  to generate slepian tapers
c  ta is a real*4 array
c
c         j. park
c
      real*8 el,a,z,pi,ww,cs,ai,an,eps,rlu,rlb
      real*8 dfac,drat,gamma,bh,ell
      common/nnaamm/iwflag
      common/npiprol/anpi
      common/tapsum/tapsum(20),ell(20)
      common/work/ip(16400)        
      common/taperzz/z(262144)  ! we use this common block for scratch space
      common/stap2/ta(16400,8)
      dimension a(16400,8),el(10)
      data iwflag/0/,pi/3.14159265358979d0/
      equivalence (a(1,1),ta(1,1))
      an=dfloat(n)
      ww=dble(anpi)/an
      cs=dcos(2.d0*pi*ww)
c initialize matrix for eispack subroutine
c      print *,'ww,cs,an',ww,cs,an
      do i=0,n-1
        ai=dfloat(i)
        a(i+1,1)=-cs*((an-1.d0)/2.d0-ai)**2
        a(i+1,2)=-ai*(an-ai)/2.d0
        a(i+1,3)=a(i+1,2)**2        ! required by eispack routine
      end do
      eps=1.e-13
      m11=1
      call tridib(n,eps,a(1,1),a(1,2),a(1,3),rlb,rlu,m11,nwin,el,ip,
     x       ierr,a(1,4),a(1,5))
c      print *,ierr,rlb,rlu
      print *,'eigenvalues for the eigentapers'
c      print *,(el(i),i=1,nwin)
      call tinvit(n,n,a(1,1),a(1,2),a(1,3),nwin,el,ip,z,ierr,
     x            a(1,4),a(1,5),a(1,6),a(1,7),a(1,8))      
c  we calculate the eigenvalues of the dirichlet-kernel problem
c  i.e. the bandwidth retention factors
c  from slepian 1978 asymptotic formula, gotten from thomson 1982 eq 2.5
c  supplemented by the asymptotic formula for k near 2n from slepian 1978 eq 61
      dfac=an*pi*ww
      drat=8.d0*dfac
      dfac=4.d0*dsqrt(pi*dfac)*dexp(-2.d0*dfac)
      do k=1,nwin
        el(k)=1.d0-dfac
        dfac=dfac*drat/k  ! is this correct formula? yes,but fails as k -> 2n
      end do
c      print *,'eigenvalues for the eigentapers (small k)'
c      print *,(el(i),i=1,nwin)
      gamma=dlog(8.d0*an*dsin(2.d0*pi*ww))+0.5772156649d0
      do k=1,nwin
        bh=-2.d0*pi*(an*ww-dfloat(k-1)/2.d0-.25d0)/gamma
        ell(k)=1.d0/(1.d0+dexp(pi*bh))
      end do
      do i=1,nwin
        el(i)=dmax1(ell(i),el(i))
      end do     
c   normalize the eigentapers to preserve power for a white process
c   i.e. they have rms value unity
      do k=1,nwin
        kk=(k-1)*n
        tapsum(k)=0.
        tapsq=0.
        do i=1,n
          aa=z(kk+i)
          ta(i,k)=aa
          tapsum(k)=tapsum(k)+aa
          tapsq=tapsq+aa*aa
        end do
        aa=sqrt(tapsq/n)
        tapsum(k)=tapsum(k)/aa
        do i=1,n
          ta(i,k)=ta(i,k)/aa
        end do
      end do
c      print *,'tapsum',(tapsum(i),i=1,nwin)
  101 format(80a)
c  refft preserves amplitudes with zeropadding
c  zum beispiel: a(i)=1.,i=1,100 will transform at zero freq to b(f=0.)=100
c  no matter how much zero padding is done
c  therefore we need not doctor the taper normalization,
c  but wait until the fft to force the desired units
      iwflag=1
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c               routine
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fft2(ar,ai,n)
c  fft routine with 2 real input arrays rather than complex - see ffttwo
c  for comments. 
c  fft2 subroutine mults by exp(i\omega t)
c  OUTPUT: f=0 in ar(1), f=f_N in ar(n/2+1)
c          ar(i)=ar(n+2-i), ai(i)=-ai(n+2-i)
c  fft2 is NOT a unitary transform, mults the series by sqrt(n)
c  the inverse FT can be effected by running fft2 on the conjugate of
c  the FFT-expansion, then taking the the conjugate of the output, 
c  and dividing thru by N. to wit:
c
c   assume Xr, Xi is in freq domain, xr, xi in the time domain
c
c   (Xr,Xi)=fft2(xr,xi,N)
c   (xr,-xi)=fft2(Xr,-Xi,N)/N
c
      implicit real*8 (a-h,o-z)
      implicit integer*4 (i-n)
      dimension ar(1),ai(1)
      mex=dlog(dble(float(n)))/.693147d0
      nv2=n/2
      nm1=n-1
      j=1
      do 7 i=1,nm1
      if(i .ge. j) go to 5
      tr=ar(j)
      ti=ai(j)
      ar(j)=ar(i)
      ai(j)=ai(i)
      ar(i)=tr
      ai(i)=ti
   5  k=nv2
   6  if(k .ge. j) go to 7
      j=j-k
      k=k/2
      go to 6
   7  j=j+k
      pi=3.14159265358979d0
      do 20 l=1,mex
      le=2**l
      le1=le/2
      ur=1.0
      ui=0.
      wr=dcos(pi/le1 )
      wi=dsin (pi/le1)
      do 20 j=1,le1
      do 10 i=j,n,le
      ip=i+le1
      tr=ar(ip)*ur - ai(ip)*ui
      ti=ai(ip)*ur + ar(ip)*ui
      ar(ip)=ar(i)-tr
      ai(ip)=ai(i) - ti
      ar(i)=ar(i)+tr
      ai(i)=ai(i)+ti
  10  continue
      utemp=ur
      ur=ur*wr - ui*wi
      ui=ui*wr + utemp*wi
  20  continue
      return
      end
      subroutine read_ttimes
      implicit real*4(a-h,o-z)
      implicit integer*4(i-n)
      common/tt_p/d_p(200),d_pkp(200),s_p(200),s_pkp(200),n_p,n_pkp
c  read in the P- and PKP-slowness values from ttimes
c  the code interpolates across the upper-mantle triplication for DELTA=19-27
c  and chooses P or PKP based on the start time of the data-analysis window
c  for DELTA.ge.105
      do i=1,200
        d_p(i)=0.
      end do
c  fill in the head-wave slowness for DELTA.lt.14
c  13.2 sec/degree is 8.4+eps km/sec
      do i=1,13
        d_p(i)=float(i)
        s_p(i)=13.2
      end do
c      open(7,file='/park/backup/Vary/Layers/ttimes_P.dat',
c     x  form='formatted')
      open(7,file='/Users/tmo22/Seismology/JeffRecFunc/ttimes_P.dat',
     x  form='formatted')
      do i=14,200
        read(7,*,end=110) d_p(i),s_p(i)
      end do
  110 close(7)
      print *,i,' = counter at end of ttimes file'
      do i=1,200
        if(d_p(i).eq.0.) go to 120
      end do
  120 n_p=i-1
c  interpolates the triplication
c  linearly interpolate slowness between Delta=17 and p=11.06 sec/degree
c                                    and Delta=28 and p=8.92 sec/degree
      do i=18,27
        d_p(i)=float(i)
        s_p(i)=11.06-(i-17)*(11.06-8.92)/11.
      end do
      do i=45,n_p
        d_p(i-18)=d_p(i)
        s_p(i-18)=s_p(i)
      end do
      n_p=n_p-18
      do i=1,200
        d_pkp(i)=0.
      end do
c      open(7,file='/park/backup/Vary/Layers/ttimes_PKP.dat',
c     x  form='formatted')
      open(7,file='/Users/tmo22/Seismology/JeffRecFunc/ttimes_PKP.dat',
     x  form='formatted')
      do i=1,200
        read(7,*,end=130) d_pkp(i),s_pkp(i)
      end do
  130 close(7)
      print *,i,' = counter at end of ttimes file'
      do i=1,200
        if(d_pkp(i).eq.0.) go to 140
      end do
  140 n_pkp=i-1
      factor=180./(3.14159265358979*6371.)
      do i=1,n_p
        s_p(i)=s_p(i)*factor
      end do
      do i=1,n_pkp
        s_pkp(i)=s_pkp(i)*factor
      end do
      print *,'n_p,n_pkp',n_p,n_pkp
c      call plotit_axes(0.,0.,0.,0.)
c      call plotit(d_p,s_p,dum,n_p,' ',' ',' ',2,0,0.0,0,0)
c      call plotit(d_pkp,s_pkp,dum,n_pkp,' ',
c     x 'Epicentral Distance (degrees)','Slowness (sec/km)',2,0,0.0,0,1)
      return
      end
