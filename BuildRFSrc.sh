#!/bin/csh -f

#  BuildRFSrc.sh
#  
#
#  Created by Tolulope  Olugboji on November 15, 2011.
#  Copyright 2011 Yale University. All rights reserved.
#
# This shell script is used to buld the RF source (JJParkCodes) for the new OSX Lion on my Mac... 
# Dependent on the BuildJlibArchive - jlib archives, and plotlib archives.
#


set DataDir = `pwd`
set AnirecSynth = `ls $DataDir/anirec_synth.f`
set SourceAll = `ls *.f`
set Jlib = `ls ../JJParkLibSource/Codes_jlib/Jlib.a`
set eislib = `ls ../plotit/eislib.a`
set plotlib = `ls ../plotit/plotlib.a`
set BuildOut = `ls ../bin/`



gfortran -o  anirec_synth $AnirecSynth $eislib $plotlib $Jlib
mv anirec_synth ../bin/
